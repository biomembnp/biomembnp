
##Plese read and cite:##

* "Au Nanoparticles in Lipid Bilayers: A Comparison between Atomistic and Coarse-Grained Models", S. Salassi et al., J. Phys. Chem. C, 2017
DOI: https://doi.org/10.1021/acs.jpcc.6b12148

* "Local enhancement of lipid membrane permeability induced by irradiated gold nanoparticles", A. Torchi et al., ACS Nano, 2017
DOI: https://doi.org/10.1021/acsnano.7b06690

* "Water dynamics affects thermal transport at the surface of hydrophobic and hydrophilic irradiated nanoparticles", S. Salassi et al., Nanoscale Adv., 2020
DOI: https://doi.org/10.1039/D0NA00094A

