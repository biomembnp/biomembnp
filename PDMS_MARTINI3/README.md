An example of PDMS topology is provided in pdms50_melt.itp , which contains the bonded parameters for a PDMS melt, made of 50-monomers chains. pdms50_melt.gro is the coordinate file of the melt. 

DMS_martini3_v1.itp contains all the non-bonded interactions between the new regular DMS bead and the other Martini beads, with the exception of the charged ones (Q beads).
Not all the interactions are already validated. The interactions relative to SDMS and TDMS will be added soon.

For more information please refer to:
"Cambiaso S, Rossi G, Bochicchio D. Development of a Transferable Coarse-Grained Model of Polydimethylsiloxane. ChemRxiv. Cambridge: Cambridge Open Engage; 2022; This content is a preprint and has not been peer-reviewed"
