/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */
#include <string>
#include <vector>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <map>
#include <gromacs/trajectoryanalysis.h>
#include <gromacs/pbcutil/pbc.h>

using namespace gmx;
using namespace std;


int modulo(int a, int b){
    int c= a%b;
    if (c<0) c+=b;
    return c;
}


class xvgPlot{

public:
void addColumn(string);
void addPoint(int, double);
void Plot(string);
void addComment(string);

private:
vector <string> column_titles;
vector <vector <double> > data;
vector <string> comments;


};


void xvgPlot::addColumn(string title){
	vector <double> column;
	data.push_back(column);
	column_titles.push_back(title);
}


void xvgPlot::addPoint(int column, double value){
	data[column].push_back(value);
}

void xvgPlot::addComment(string comment){
	comments.push_back("# "+comment);
}

void xvgPlot::Plot(string filename){

	FILE * output_file=fopen(filename.c_str(),"w");

	int n_columns= data.size();

	int n_rows=data[0].size();
	for (int i = 1; i < data.size(); ++i)  {
		if(data[i].size()!=n_rows) {
			fprintf(stderr,"xvgPlot: ERROR ! COLUMNS HAVE DIFFERENT NUMBER OF ELEMENTS.");
				exit(1);
		}
	}

	for(int i=0;i<comments.size();i++) fprintf(output_file,"%s\n",comments[i].c_str());

	for(int i=0;i<column_titles.size();i++) fprintf(output_file,"# column %i = %s\n",i,column_titles[i].c_str());


	for (int i = 0; i < n_rows; ++i)
	{
		for (int j = 0; j < n_columns; ++j)
		{
			fprintf(output_file,"%f \t\t",(float)data[j][i]);
		}
		fprintf(output_file,"\n");
	}

	fclose(output_file);

}



typedef struct crossing_data{
vector <double> pos_enter;
double t_enter;
double dist_enter;
} crossing_data;


class cubic_box_adjust_to_pbc{    //it is implemented as a class to avoid passing box parameters at every function call
    public:
        cubic_box_adjust_to_pbc(vector<double>,vector<double>);
        vector <double> box;
        vector <double> center;
        vector <double> adjust(vector<double>);
};

cubic_box_adjust_to_pbc::cubic_box_adjust_to_pbc(vector<double> box1, vector <double> center1){
    box=box1;
    center=center1;
}


vector <double> cubic_box_adjust_to_pbc::adjust(vector <double> old_pos){
    vector<double> new_pos(3);

    //adjusting positions for boundary conditions (TO DO: MUST CHECK!)
    for (int i = 0; i < 3; ++i)
    {
        new_pos[i] = old_pos[i] - center[i] + 0.5*box[i];
        while(new_pos[i] >= box[i]) new_pos[i]-=box[i];
        while(new_pos[i] < 0) new_pos[i]+=box[i];
    }

    return new_pos;
}



class cubic_box_section_selector{  //it is implemented as a class to avoid passing box parameters at every function call
    public:
        cubic_box_section_selector(vector <double> , int, int);
        int get_section_x(double);
        int get_section_y(double);
        int n_sections_x,n_sections_y;
        double section_side_x, section_side_y;
};

cubic_box_section_selector::cubic_box_section_selector(vector <double> box ,int n_sections_x_1,int n_sections_y_1){
    n_sections_x=n_sections_x_1;
    n_sections_y=n_sections_y_1;
    section_side_x=box[0]/(double)n_sections_x;
    section_side_y=box[1]/(double)n_sections_y;
}

int cubic_box_section_selector::get_section_x(double x){
    int section_x = (int)(x/section_side_x);
    if(section_x >=0 && section_x<n_sections_x) return section_x; else return -1;
}

int cubic_box_section_selector::get_section_y(double y){
    int section_y = (int)(y/section_side_y);
    if(section_y >=0 && section_y<n_sections_y) return section_y; else return -1;
}



string int_to_string(int a, int padding){
    ostringstream stream1;
    stream1<<setfill('0')<<setw(5)<<a;
    return stream1.str();
}

typedef struct thresholds_set{
	double water_down;
	double membrane_down;
	double membrane_up;
	double water_up;
} thresholds_set;


class lipid_grid_matrix {

public:
lipid_grid_matrix(int,int);
vector< vector <thresholds_set> > compute_thresholds_matrix(double, float);
int n_sections_x;
int n_sections_y;
vector <vector <vector <double> > > grid_matrix;

};

lipid_grid_matrix :: lipid_grid_matrix(int nx, int ny){
	n_sections_x=nx;
	n_sections_y=ny;

	// initializing lipid grid matrix
    vector <double> lipid_grid_element;
    vector <vector <double> > lipid_grid_row;
    for (int i = 0; i < n_sections_x; ++i) lipid_grid_row.push_back(lipid_grid_element);
    for (int j = 0; j < n_sections_y; ++j) grid_matrix.push_back(lipid_grid_row);
}


vector< vector <thresholds_set> > lipid_grid_matrix :: compute_thresholds_matrix(double water_buffer_thickness, float thickness_reduction_tolerance){

	// initializing thresholds grid matrix
    vector <vector <thresholds_set> > thresholds_grid_matrix;
    vector <thresholds_set>  thresholds_grid_row(n_sections_x);
    for (int j = 0; j < n_sections_y; ++j) thresholds_grid_matrix.push_back(thresholds_grid_row);

    double thickness_sum=0;

    for (int i = 0; i < n_sections_x; ++i)   {
    	for (int j = 0; j < n_sections_y; ++j)   {

            int n_atoms_sector= grid_matrix.at(i).at(j).size();

            if(n_atoms_sector>1){
                //calculate mean of all z coordinates in grid element (z coordinate of geometric center)
                double GC_global = accumulate ( grid_matrix.at(i).at(j).begin( ) , grid_matrix.at(i).at(j).end( ) , 0.0 ) / (double)n_atoms_sector;

                //calculate geometric centers of atoms above and below the global geometric center
                vector <double> list_up;
                vector <double> list_down;
                for(int k = 0 ; k < n_atoms_sector ; k++) if(grid_matrix.at(i).at(j)[k]<GC_global) list_down.push_back(grid_matrix.at(i).at(j)[k]); else list_up.push_back(grid_matrix.at(i).at(j)[k]);
                double GC_up   = accumulate (   list_up.begin( ) ,   list_up.end( ) , 0.0 ) /   (double)list_up.size();
                double GC_down = accumulate ( list_down.begin( ) , list_down.end( ) , 0.0 ) / (double)list_down.size();

                //assign membrane threshold values
                thresholds_grid_matrix.at(i).at(j).membrane_down = GC_down ;
                thresholds_grid_matrix.at(i).at(j).membrane_up =   GC_up ;
                thickness_sum += GC_up - GC_down ;
            } else{  // if there is not enough data
                thresholds_grid_matrix.at(i).at(j).membrane_down = 0 ;
                thresholds_grid_matrix.at(i).at(j).membrane_up =   0 ;
            }

    	}
    }

    double average_thickness = thickness_sum / (double)(n_sections_x*n_sections_y);

    bool flag_repeat=true;
     int diagnostic_counter=0;

    while(flag_repeat){
    flag_repeat=false;
    for (int i = 0; i < n_sections_x; ++i)   {
    	for (int j = 0; j < n_sections_y; ++j)   {

             // check for zones with insufficient data (thickness = 0) or single leaflet (lower thickness).
            if(average_thickness - (thresholds_grid_matrix.at(i).at(j).membrane_up - thresholds_grid_matrix.at(i).at(j).membrane_down)  > average_thickness*thickness_reduction_tolerance ){
                double sum_neighbourhood_up=0;
                double sum_neighbourhood_down=0;
                double valid_neighbours_count=0;
                for(int shift_x=-1;shift_x<2;shift_x++){
                    for(int shift_y=-1;shift_y<2;shift_y++){
                            if(shift_x != 0 || shift_y != 0){
                                    int x_index = modulo((i+ shift_x),n_sections_x);  //boundary conditions are taken into account
                                    int y_index = modulo((j+ shift_y),n_sections_y);  //boundary conditions are taken into account
                                    double z_up = thresholds_grid_matrix.at(x_index).at(y_index).membrane_up;
                                    double z_down = thresholds_grid_matrix.at(x_index).at(y_index).membrane_down;
                                    double thickness =  z_up - z_down;
                                    if ( average_thickness - thickness < average_thickness * thickness_reduction_tolerance ) {
                                         sum_neighbourhood_up += z_up;
                                         sum_neighbourhood_down += z_down;
                                         valid_neighbours_count++;
                                    }
                            }
                   }
                }

                if( valid_neighbours_count > 0 ) {
                    thresholds_grid_matrix.at(i).at(j).membrane_up   = sum_neighbourhood_up   / valid_neighbours_count;
                    thresholds_grid_matrix.at(i).at(j).membrane_down = sum_neighbourhood_down / valid_neighbours_count;
                } else {
                     flag_repeat = true;   //if some missing threshold value cannot be reassigned, then the process has to be repeated.
                }

            }


    	}
    }
    }

     //assign water buffer boundaries.
        for (int i = 0; i < n_sections_x; ++i)   {
            for (int j = 0; j < n_sections_y; ++j)   {
                thresholds_grid_matrix.at(i).at(j).water_down =   thresholds_grid_matrix.at(i).at(j).membrane_down - water_buffer_thickness;
                thresholds_grid_matrix.at(i).at(j).water_up   =   thresholds_grid_matrix.at(i).at(j).membrane_up   + water_buffer_thickness;
            }
        }



    return thresholds_grid_matrix;

}






/*! \brief
 * Template class to serve as a basis for user analysis tools.
 */
class AnalysisTemplate : public TrajectoryAnalysisModule
{
    public:
        AnalysisTemplate();

        virtual void initOptions(Options                    *options,
                                 TrajectoryAnalysisSettings *settings);
        virtual void initAnalysis(const TrajectoryAnalysisSettings &settings,
                                  const TopologyInformation        &top);

        virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                                  TrajectoryAnalysisModuleData *pdata);

        virtual void finishAnalysis(int nframes);
        virtual void writeOutput();

    private:
        class ModuleData;

    string 		crossings_counter_file, average_thickness_file;
    string 		log_file, data_file;
    ofstream 	log_file_stream;
    Selection 	lipid_sel_;
    Selection 	water_sel_;
    Selection 	center_sel_;
    int 	n_sections_x;
    int 	n_sections_y;
    float  water_buffer_thickness;
    float  z_offset_center;
    float relative_thickness_threshold;
    vector <int> previous_buffer_up_list, previous_membrane_list, previous_buffer_down_list;
    map <int,char> water_direction;
     int entered_upwards;
	 int entered_downwards;
	 int exited_upwards;
	 int exited_downwards;
	 int crossed_upwards;
	 int crossed_downwards;
	 bool first_frame;
	 bool ref_centering;
    map <int,crossing_data> crossings_data;
	 xvgPlot crossings_data_plot;
	 vector <vector <double> > matrix_sum_thickness;

    AnalysisData                     data_;
    AnalysisDataAverageModulePointer avem_;
};


AnalysisTemplate::AnalysisTemplate()
    : TrajectoryAnalysisModule("lipid_permeation", "Template analysis tool")
{
    registerAnalysisDataset(&data_, "lipid_permeation");
}


void
AnalysisTemplate::initOptions(Options                    *options,
                              TrajectoryAnalysisSettings *settings)
{
    static const char *const desc[] = {
        "This is a template for writing your own analysis tools for",
        "GROMACS. The advantage of using GROMACS for this is that you",
        "have access to all information in the topology, and your",
        "program will be able to handle all types of coordinates and",
        "trajectory files supported by GROMACS. In addition,",
        "you get a lot of functionality for free from the trajectory",
        "analysis library, including support for flexible dynamic",
        "selections. Go ahead an try it![PAR]",
        "To get started with implementing your own analysis program,",
        "follow the instructions in the README file provided.",
        "This template implements a simple analysis programs that calculates",
        "average distances from a reference group to one or more",
        "analysis groups."
    };


    data_file = "lipid_permeation_data_file.xvg";
    average_thickness_file = "average_thickness";

    options->setDescription(desc);

    options->addOption(BooleanOption("center")
                            .store(&ref_centering)
                            .defaultValue(true)
                            .description("Center the trajectory data on a given reference group and calculate planar distance of permeation events."));

    options->addOption(FileNameOption("o")
                           .filetype(eftPlot).outputFile()
                           .store(&data_file)
                           .description("Lipid permeation data file"));

    options->addOption(FileNameOption("oc")
                           .filetype(eftPlot).outputFile()
                           .store(&crossings_counter_file).defaultBasename("crossings_counter").required()
                           .description("Crossings counter data file"));


    options->addOption(FileNameOption("tmap")
                           .filetype(eftPlot).outputFile()
                           .store(&average_thickness_file).defaultBasename("average_thickness").required()
                           .description("Average membrane thickness map"));

    options->addOption(FileNameOption("log")
                           .filetype(eftPlot).outputFile()
                           .store(&log_file).defaultBasename("lipid_permeation_log").required()
                           .description("Lipid permeation log file"));

    options->addOption(SelectionOption("lipid_group")
                           .store(&lipid_sel_)
                           .required()
                           .description("Group containing all lipids"));

    options->addOption(SelectionOption("water_oxygen_group")
                           .store(&water_sel_)
                           .required()
                           .description("Group containing all oxygen atoms of water molecules"));


    if (ref_centering) options->addOption(SelectionOption("center_group")      //this must become optional
                           .store(&center_sel_)
                           .required()
                           .description("Group for centering"));


    options->addOption(IntegerOption("sections_side_x")
    						.store(&n_sections_x)
    						.required()
    						.description("Number of sections per x side"));

    options->addOption(IntegerOption("sections_side_y")
    						.store(&n_sections_y)
    						.required()
    						.description("Number of sections per y side"));

    options->addOption(FloatOption("water_buffer_thickness")
    					.store(&water_buffer_thickness)
    					.required()
    					.description("Thickness (nm) of water buffer on each side of the membrane"));


	 options->addOption(FloatOption("z_offset_center")
    					.store(&z_offset_center)
    					.defaultValue(0.0)
    					.description("Center offset (nm) along z axis (positive values -> center group shifted up)"));

     options->addOption(FloatOption("relative_thickness_threshold")
    					.store(&relative_thickness_threshold)
                        .defaultValue(0.2)
    					.description("Relative thickness threshold for membrane"));

}


void
AnalysisTemplate::initAnalysis(const TrajectoryAnalysisSettings &settings,
                               const TopologyInformation         & /*top*/)
{

	data_.setColumnCount(0, 2);

    avem_.reset(new AnalysisDataAverageModule());
    data_.addModule(avem_);

    if (!crossings_counter_file.empty())
    {
        AnalysisDataPlotModulePointer plotm(
                new AnalysisDataPlotModule(settings.plotSettings()));
        plotm->setFileName(crossings_counter_file);
        plotm->setTitle("Membrane permeation");
        plotm->setXAxisIsTime();
        plotm->setYLabel("Crossings");
        plotm->appendLegend("Upwards");
        plotm->appendLegend("Downwards");
        data_.addModule(plotm);
    }


    //initializing log file
    log_file_stream.open(log_file.c_str());

    //initializing crossings data plot;
    crossings_data_plot.addColumn("atom index");
    crossings_data_plot.addColumn("Entry time");
    crossings_data_plot.addColumn("Entry coordinate x");
    crossings_data_plot.addColumn("Entry coordinate y");
    crossings_data_plot.addColumn("Entry coordinate z");
    crossings_data_plot.addColumn("Exit time");
    crossings_data_plot.addColumn("Exit coordinate x");
    crossings_data_plot.addColumn("Exit coordinate y");
    crossings_data_plot.addColumn("Exit coordinate z");
    crossings_data_plot.addColumn("Crossing direction (1=upwards, -1=downwards)");
     if (ref_centering) crossings_data_plot.addColumn("Entry xy distance from center");
     if (ref_centering) crossings_data_plot.addColumn("Exit  xy distance from center");

    // printing analysis parameters on screen and log file
    cout<<"ANALYSIS PARAMETERS:"<<endl;
    cout<<"Lipid permeation data file: "<<data_file<<endl;
    cout<<"Crossings counter file: "<<crossings_counter_file<<endl;
    cout<<"Average thickness file: "<<average_thickness_file<<endl;
    cout<<"Lipid permeation log file: "<<log_file<<endl;
    cout<<"Group containing all lipids: "<<lipid_sel_.name()<<endl;
    cout<<"Group containing all water molecules: "<<water_sel_.name()<<endl;
     if (ref_centering) cout<<"Group for centering: "<<center_sel_.name()<<endl;
    cout<<"z offset for centering: "<<z_offset_center<< " nm" <<endl;
    cout<< "Number of sections per x side: "<<n_sections_x<<endl;
    cout<< "Number of sections per y side: "<<n_sections_y<<endl;
    cout<< "Thickness of water buffer on each side of the membrane: "<< water_buffer_thickness<< " nm" <<endl;
    cout<< "Relative thickness threshold for membrane: " << relative_thickness_threshold <<endl;
    cout<<endl<<endl;

    log_file_stream<<"ANALYSIS PARAMETERS:"<<endl;
    log_file_stream<<"Lipid permeation data file: "<<data_file<<endl;
    log_file_stream<<"Crossings counter file: "<<crossings_counter_file<<endl;
    log_file_stream<<"Average thickness file: "<<average_thickness_file<<endl;
    log_file_stream<<"Lipid permeation log file: "<<log_file<<endl;
    log_file_stream<<"Group containing all lipids: "<<lipid_sel_.name()<<endl;
    log_file_stream<<"Group containing all water molecules: "<<water_sel_.name()<<endl;
     if (ref_centering) log_file_stream<<"Group for centering: "<<center_sel_.name()<<endl;
    log_file_stream<<"z offset for centering: "<<z_offset_center<< " nm" <<endl;
    log_file_stream<< "Number of sections per x side: "<<n_sections_x<<endl;
    log_file_stream<< "Number of sections per y side: "<<n_sections_y<<endl;
    log_file_stream<< "Thickness of water buffer on each side of the membrane: "<< water_buffer_thickness<< " nm" <<endl;
    log_file_stream<< "Relative thickness threshold for membrane: " << relative_thickness_threshold <<endl;
    log_file_stream<<endl<<endl;


	 //initializing output parameters
	 entered_upwards=0;
	 entered_downwards=0;
	 exited_upwards=0;
	 exited_downwards=0;
	 crossed_upwards=0;
	 crossed_downwards=0;

	//initializing miscellaneous parameters
	first_frame=true;
	matrix_sum_thickness.resize(n_sections_x, vector <double> (n_sections_y, 0.0));
}


void
AnalysisTemplate::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                               TrajectoryAnalysisModuleData *pdata)
{

    AnalysisDataHandle         dh     = pdata->dataHandle(data_);
    const Selection           &lipid_sel =  pdata->parallelSelection(lipid_sel_);
    const Selection           &water_sel =  pdata->parallelSelection(water_sel_);
	const Selection           &center_sel = pdata->parallelSelection(center_sel_);

    dh.startFrame(frnr, fr.time);

    //writing frame to log file
    log_file_stream<<"Frame "<<frnr<<endl;

    //extracting (cubic) box dimensions
    vector <double> box(3);
    for (int i = 0; i < 3; ++i) box.at(i)=pbc->box[i][i];

    //calculating geometric center of center group, keeping periodic boundary conditions into account.
    // WARNING: THIS METHOD WORKS ONLY IF THE DISTANCE BETWEEN ANY ATOMS IN THE CENTER GROUP IS SMALLER THAN ALL BOX LENGTHS.
	 int n_center_atoms = center_sel.posCount();
	 vector <double> cgroup_pos1;  //position of first atom in the center group.
	 for(int i=0  ; i< 3  ;  i++) cgroup_pos1.push_back(center_sel.position(0).x()[i]) ;

	 		//initializing temporary center adjuster
	 		cubic_box_adjust_to_pbc temporary_center_pbc_adjuster(box,cgroup_pos1);  //adjusts any position to a box centered on the first atom of the center group, so that the geometric center can be calculated correctly.

            //calculating temporary geometric center
			vector <double> temporary_gc;
			for(int j=0  ; j< 3  ;  j++)  temporary_gc.push_back(0);
			for(int i=0  ; i<n_center_atoms ; i++) {
				SelectionPosition pos_atom_center= center_sel.position(i);
				vector <double> position_vector,temporary_adjusted_position;
				for(int j=0  ; j< 3  ;  j++) position_vector.push_back(pos_atom_center.x()[j]);
				temporary_adjusted_position= temporary_center_pbc_adjuster.adjust(position_vector);
				for(int j=0  ; j< 3  ;  j++)  temporary_gc.at(j) += temporary_adjusted_position.at(j) ;
			}
			for(int j=0  ; j< 3  ;  j++)  temporary_gc.at(j) /= n_center_atoms;
			vector <double> temporary_gc_with_offset= temporary_gc;
			temporary_gc_with_offset.at(2) = temporary_gc.at(2) - z_offset_center;

		  //initializing center adjuster
		   vector <double> gc_old_reference(3);
		   for(int i=0; i<3; i++) gc_old_reference[i] = temporary_gc_with_offset.at(i) + cgroup_pos1.at(i) - box.at(i)/2.0;
           cubic_box_adjust_to_pbc center_pbc_adjuster(box, gc_old_reference);  //using this adjustment, geometric center has position (0,0,z_offset_center)


		// fail-safe test for centering ; defining center position.

		vector <double> center_pos_without_offset, center_pos;
		for(int j=0  ; j < 3  ;  j++)  center_pos_without_offset.push_back(0);

		for(int i=0  ; i<n_center_atoms ; i++) {
			vector <double>  pos1(3),pos1_adjusted;
			for(int j=0  ; j< 3  ;  j++)  pos1.at(j) = center_sel.position(i).x()[j];
			pos1_adjusted=center_pbc_adjuster.adjust(pos1);
			for(int j=0  ; j< 3  ;  j++)  center_pos_without_offset.at(j) += pos1_adjusted.at(j);

		}
		for(int j=0  ; j< 3  ;  j++)  center_pos_without_offset.at(j) /= n_center_atoms;

		center_pos=center_pos_without_offset;
		center_pos.at(2) = center_pos_without_offset.at(2) - z_offset_center;

		for(int i=0;i<3;i++)  {
			if(fabs(center_pos.at(i)-0.5*box.at(i))>1e-5) {
				cout << "PROGRAM MALFUNCTION : CENTERING ERROR"<<endl;
				cout << "Center mismatch: "<<center_pos.at(0)-0.5*box.at(0) << "\t"<<center_pos.at(1)-0.5*box.at(1) << "\t"<<center_pos.at(2)-0.5*box.at(2) << endl;
                log_file_stream << "PROGRAM MALFUNCTION : CENTERING ERROR" << endl;
				log_file_stream << "Center mismatch: " << center_pos.at(0)-0.5*box.at(0) << "\t"<<center_pos.at(1)-0.5*box.at(1) << "\t"<<center_pos.at(2)-0.5*box.at(2) << endl;
				return;
			}
		}



     /*
     else {
            vector <double> box_center = box;
            for(int i=0; i<3; i++) box_center[i] = box[i] / 2.0 ;
            center_pbc_adjuster = cubic_box_adjust_to_pbc(box, box_center);
     }
     */


    //initializing section selector
    cubic_box_section_selector section_selector(box, n_sections_x, n_sections_y);

    //calculating grid element dimensions
    double grid_element_side_x = box[0]/(double)n_sections_x;
    double grid_element_side_y = box[1]/(double)n_sections_y;

    //initializing lipid grid
    lipid_grid_matrix lipid_grid(n_sections_x,n_sections_y);

    //assigning lipid atoms to lipid grid elements
    int n_lipid_atoms= lipid_sel.posCount();
    for (int g = 0; g < n_lipid_atoms; ++g)  {
        SelectionPosition p = lipid_sel.position(g);
        vector <double> r;
        for (int i = 0; i < 3; ++i) r.push_back(p.x()[i]);
        vector <double> adjusted_x = center_pbc_adjuster.adjust(r);
		  int section_x = section_selector.get_section_x(adjusted_x[0]);
		  int section_y = section_selector.get_section_y(adjusted_x[1]);
        if(section_x==-1 || section_y==-1) {
        		cout<<"ATOM "<<g<<" OUT OF BOUNDARY:"<<"  x="<<adjusted_x[0]<<"  y="<<adjusted_x[1]<<"  z="<<adjusted_x[2]<<endl;
        		log_file_stream <<"ATOM "<<g<<" OUT OF BOUNDARY:"<<"  x="<<adjusted_x[0]<<"  y="<<adjusted_x[1]<<"  z="<<adjusted_x[2]<<endl;
        } else{
            lipid_grid.grid_matrix.at(section_x).at(section_y).push_back(adjusted_x[2]);
        }

    }

    vector< vector <thresholds_set> > thresholds_matrix = lipid_grid.compute_thresholds_matrix(water_buffer_thickness, relative_thickness_threshold);

    //add thickness data to average thickness matrix
    for (int i = 0; i < n_sections_x; ++i) {
        for (int j = 0; j < n_sections_y; ++j) {
            double membrane_thickness = thresholds_matrix.at(i).at(j).membrane_up - thresholds_matrix.at(i).at(j).membrane_down;
            matrix_sum_thickness.at(i).at(j) += membrane_thickness;
        }
    }

	//assigning water oxygen atoms to sections and lists
	vector<vector <double> > water_adjusted_x ;
	int n_oxygen_atoms= water_sel.posCount();
	for (int g = 0; g < n_oxygen_atoms; ++g)  {
 		SelectionPosition p = water_sel.position(g);
	   vector <double> x;
      for (int i = 0; i < 3; ++i) x.push_back(p.x()[i]);
      water_adjusted_x.push_back(center_pbc_adjuster.adjust(x));
   }
	vector <int> buffer_up_list, membrane_list, buffer_down_list;
	for (int g = 0; g < n_oxygen_atoms; ++g)  {
        int section_x = section_selector.get_section_x(water_adjusted_x[g][0]);
		  int section_y = section_selector.get_section_y(water_adjusted_x[g][1]);
        if(section_x==-1 || section_y==-1) {
        		cout<<"ATOM "<<g<<" OUT OF BOUNDARY:"<<"  x="<<water_adjusted_x[g][0]<<"  y="<<water_adjusted_x[g][1]<<"  z="<<water_adjusted_x[g][2]<<endl;
        		log_file_stream<<"ATOM "<<g<<" OUT OF BOUNDARY:"<<"  x="<<water_adjusted_x[g][0]<<"  y="<<water_adjusted_x[g][1]<<"  z="<<water_adjusted_x[g][2]<<endl;
        } else{
        		double z = water_adjusted_x[g][2];
        		double water_down= thresholds_matrix.at(section_x).at(section_y).water_down;
        		double membrane_down= thresholds_matrix.at(section_x).at(section_y).membrane_down;
        		double membrane_up= thresholds_matrix.at(section_x).at(section_y).membrane_up;
        		double water_up= thresholds_matrix.at(section_x).at(section_y).water_up;
        		if( z>water_down && z<=membrane_down )  buffer_down_list.push_back(g);
        		else if ( z>=membrane_down && z<=membrane_up )  membrane_list.push_back(g);
        		else if ( z>membrane_up && z<water_up ) buffer_up_list.push_back(g);
        		if(membrane_down<0 || membrane_up> box[2]) log_file_stream<< "THRESHOLDS AT ("<<section_x<<";"<<section_y<<") OUT OF BOUNDARIES"<<endl;

        }



    }

    //compiling lists of newly entered and newly exited oxygen atoms in membrane interval. IMPORTANT: lists are already sorted, OTHERWISE THEY MUST BE SORTED

    vector<int> entered(membrane_list.size());
    vector<int>::iterator it1;
    it1= set_difference (membrane_list.begin(), membrane_list.end(), previous_membrane_list.begin(), previous_membrane_list.end(), entered.begin());
    entered.resize(it1-entered.begin());

    vector<int> exited(membrane_list.size());
    vector<int>::iterator it2;
    it2= set_difference (previous_membrane_list.begin(), previous_membrane_list.end(), membrane_list.begin(), membrane_list.end(), exited.begin());
    exited.resize(it2-exited.begin());

    //determining transfer direction of entering and exiting oxygens

	for(int i=0  ; i< entered.size()  ;  i++)  {
		if(find(previous_buffer_up_list.begin(), previous_buffer_up_list.end(), entered.at(i)) != previous_buffer_up_list.end() ) {
			entered_downwards++;
			water_direction[entered.at(i)] = 'd';
			crossing_data data1;
			data1.pos_enter=water_adjusted_x[entered.at(i)];
			data1.t_enter=fr.time;
			data1.dist_enter=sqrt((data1.pos_enter[0]-center_pos[0])*(data1.pos_enter[0]-center_pos[0]) + (data1.pos_enter[1]-center_pos[1])*(data1.pos_enter[1]-center_pos[1]));
			crossings_data.insert(pair<int,crossing_data>(entered.at(i),data1));
		}
		else if (find(previous_buffer_down_list.begin(), previous_buffer_down_list.end(), entered.at(i)) != previous_buffer_down_list.end() )  {
			entered_upwards++;
			water_direction[entered.at(i)] = 'u';
			crossing_data data1;
			data1.pos_enter=water_adjusted_x[entered.at(i)];
			data1.t_enter=fr.time;
			data1.dist_enter=sqrt((data1.pos_enter[0]-center_pos[0])*(data1.pos_enter[0]-center_pos[0]) + (data1.pos_enter[1]-center_pos[1])*(data1.pos_enter[1]-center_pos[1]));
			crossings_data.insert(pair<int,crossing_data>(entered.at(i),data1));
		}
		else {
			if(!first_frame){
				cout<<"ERROR: OXYGEN ATOM "<<water_sel.atomIndices()[entered.at(i)]<<" ENTERED IN MEMBRANE UNEXPECTEDLY ;"<<endl;
				log_file_stream<<"ERROR: OXYGEN ATOM "<<water_sel.atomIndices()[entered.at(i)]<<" ENTERED IN MEMBRANE UNEXPECTEDLY ;"<<endl;
			}
		}
	}

	for(int i=0  ; i< exited.size()  ;  i++)  {
		if(find(buffer_up_list.begin(), buffer_up_list.end(), exited.at(i)) != buffer_up_list.end() ) {
			exited_upwards++;
			if(water_direction[exited.at(i)]=='u') {
			    crossed_upwards++;
			    int index_exited=exited.at(i);
			    log_file_stream<<"ATOM "<< water_sel.atomIndices()[index_exited] <<" HAS CROSSED THE MEMBRANE UPWARDS" << endl;
				 crossing_data data1= crossings_data[index_exited];
				 crossings_data_plot.addPoint(0,water_sel.atomIndices()[index_exited]);
				 crossings_data_plot.addPoint(1,data1.t_enter);
			    crossings_data_plot.addPoint(2,data1.pos_enter[0]);
			    crossings_data_plot.addPoint(3,data1.pos_enter[1]);
			    crossings_data_plot.addPoint(4,data1.pos_enter[2]);
			    crossings_data_plot.addPoint(5,fr.time);
			    crossings_data_plot.addPoint(6, water_adjusted_x[index_exited][0] );
			    crossings_data_plot.addPoint(7, water_adjusted_x[index_exited][1] );
			    crossings_data_plot.addPoint(8, water_adjusted_x[index_exited][2] );
			    crossings_data_plot.addPoint(9,1);
			    if(ref_centering) crossings_data_plot.addPoint(10, data1.dist_enter );
			    double delta_x = water_adjusted_x[index_exited][0] - center_pos[0] ;
			    double delta_y = water_adjusted_x[index_exited][1] - center_pos[1] ;
			    if(ref_centering) crossings_data_plot.addPoint(11,  sqrt(delta_x*delta_x + delta_y*delta_y) );
             crossings_data.erase(index_exited);
			}
			crossings_data.erase(exited.at(i));
		}
		else if (find(buffer_down_list.begin(), buffer_down_list.end(), exited.at(i)) != buffer_down_list.end() )  {
			exited_downwards++;
			if(water_direction[exited.at(i)]=='d') {
			    crossed_downwards++;
				 int index_exited=exited.at(i);
			    log_file_stream<<"ATOM "<< water_sel.atomIndices()[index_exited] <<" HAS CROSSED THE MEMBRANE DOWNWARDS" << endl;
			    crossing_data data1= crossings_data[index_exited];
                crossings_data_plot.addPoint(0,water_sel.atomIndices()[index_exited]);
                crossings_data_plot.addPoint(1,data1.t_enter);
			    crossings_data_plot.addPoint(2,data1.pos_enter[0]);
			    crossings_data_plot.addPoint(3,data1.pos_enter[1]);
			    crossings_data_plot.addPoint(4,data1.pos_enter[2]);
			    crossings_data_plot.addPoint(5,fr.time);
			    crossings_data_plot.addPoint(6, water_adjusted_x[index_exited][0] );
			    crossings_data_plot.addPoint(7, water_adjusted_x[index_exited][1] );
			    crossings_data_plot.addPoint(8, water_adjusted_x[index_exited][2] );
			    crossings_data_plot.addPoint(9,-1);
			    if(ref_centering) crossings_data_plot.addPoint(10, data1.dist_enter );
			    double delta_x = water_adjusted_x[index_exited][0] - center_pos[0] ;
			    double delta_y = water_adjusted_x[index_exited][1] - center_pos[1] ;
			    if(ref_centering) crossings_data_plot.addPoint(11,  sqrt(delta_x*delta_x + delta_y*delta_y) );
          }
            crossings_data.erase(exited.at(i));

		}
		else {
			cout<<"ERROR: OXYGEN ATOM "<<exited.at(i)<<" EXITED FROM MEMBRANE UNEXPECTEDLY;"<<endl;
			log_file_stream<<"ERROR: OXYGEN ATOM "<<exited.at(i)<<" EXITED FROM MEMBRANE UNEXPECTEDLY;"<<endl;
		}
		water_direction.erase(exited.at(i));
	}


	//updating previous lists
    previous_membrane_list=membrane_list;
    previous_buffer_up_list=buffer_up_list;
    previous_buffer_down_list=buffer_down_list;
    dh.setPoint(0,crossed_upwards);
	dh.setPoint(1,crossed_downwards);

    dh.finishFrame();

    first_frame=false;
}


void
AnalysisTemplate::finishAnalysis(int nframes)
{
	log_file_stream.close();
	crossings_data_plot.Plot(data_file);


	//writing file with matrix of average membrane thickness
    ofstream thickness_matrix_file;
    thickness_matrix_file.open(average_thickness_file.c_str());
    for (int i = 0; i < n_sections_x; ++i) {
        for (int j = 0; j < n_sections_y; ++j) {
            double membrane_thickness = matrix_sum_thickness.at(i).at(j) / nframes;
            thickness_matrix_file << setprecision(3) << setfill(' ') << setw(5) << membrane_thickness << "  " ;
        }
        thickness_matrix_file << endl;
    }
    thickness_matrix_file.close();



}


void
AnalysisTemplate::writeOutput()
{

}

/*! \brief
 * The main function for the analysis template.
 */
int
main(int argc, char *argv[])
{
    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<AnalysisTemplate>(argc, argv);
}
