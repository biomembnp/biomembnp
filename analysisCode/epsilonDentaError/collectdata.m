%%-------------------------------------------------------------------------
% Copyright (C)  2013 Eliodoro Chiavazzo & Pietro Asinari
%
% This file is part of DeltaSasComputation package.

% Please remember to cite:
% Chiavazzo E., Fasano M., Asinari P. and Decuzzi P., Scaling Behavior for the Water Transport in Nanoconfined Geometries, Nature Communications, (2014)

% DeltaSasComputation package is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% DeltaSasComputation package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with DeltaSasComputation package.  If not, see <http://www.gnu.org/licenses/>.
%%-------------------------------------------------------------------------

% This subroutine reads data from GROMACS i) topology file (*.top); ii) g_sas output file (*.xvg);
% and iii) non-bonded force field file (*.itp). As an output, it contructs a matrix and a few parameters that will be used by the main script (DeltaSasComputation.m)   
%
%Matrice:= n, sigma, epsilon, charge 
function [MATRICE,Natoms,C6oxy,C12oxy]=collectdata

TOPFILE='topfile.top';    % File used in GROMACS, where the particle topology is specified
XVGFILE='atomarea.xvg';    % File produced by the GROMACS command g_sas (see also http://manual.gromacs.org/online/g_sas.html)
ITPFILE='nonbonded.itp';    % Force field file of non-bonded interactions

Ntypes=17;                % Total number of atomtypes in your .itp file 
wateroxy='OW';            % Name assigned to the oxygen forming water  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CLEAN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TOPFILE2=strcat(TOPFILE,'.clean');  

fid = fopen(TOPFILE,'r');
fid2 = fopen(TOPFILE2,'w');

tline = fgetl(fid);
cont=0;

while ischar(tline)
    cont=cont+1;
    line_string = sprintf('%s',tline);
    u = strfind(line_string, '; residue');
    
    if isempty(u)
        fprintf(fid2,'%s',tline);
        fprintf(fid2,'\n');
    end
    
    tline = fgetl(fid); %go to next line
end
 
fclose('all');

TOPFILE=TOPFILE2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ATOMS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen(TOPFILE,'r');
 
tline = fgetl(fid);
cont=0;

while ischar(tline) 
    cont=cont+1;
    line_string = sprintf('%s',tline);
    u = strfind(line_string, '[atoms]');
    
    if u==1
        break
    end
    
    tline = fgetl(fid); %go to next line
end
 
fclose('all');

inizioATOMS=cont+1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%BONDS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen(TOPFILE,'r');
 
tline = fgetl(fid);
cont=0;

while ischar(tline)
    cont=cont+1;
    line_string = sprintf('%s',tline);
    u = strfind(line_string, '[bonds]');
    if u==1
        break
    end
    tline = fgetl(fid); %go to next line
end

fclose('all');

fineATOMS=cont-2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%BONDS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Natoms=fineATOMS-inizioATOMS+1;
MATRICE=zeros(Natoms,6);

%disp(Natoms)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen(TOPFILE,'r');

for i=1:inizioATOMS-1
    tline=fgets(fid); 
end 

for i=1:fineATOMS-inizioATOMS+2
    riga=textscan(fid,'%n %s %n %s %s %n %f',1);
    ATOMS{i}=riga;
    tline = fgetl(fid); %go to next line
end 

fclose('all');

for i=1:fineATOMS-inizioATOMS+1
    MATRICE(i,1)=ATOMS{i}{1};
    MATRICE(i,4)=ATOMS{i}{7};
    TYPES{i}=ATOMS{i}{2};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen(XVGFILE,'r');

tline = fgetl(fid);
cont=0;

while ischar(tline) 
    cont=cont+1;
    line_string = sprintf('%s',tline);
    u = strfind(line_string, '@ s1');
    
    if u==1
        break
    end
    
    tline = fgetl(fid); %go to next line
end

for i=1:Natoms
    riga=textscan(fid,'%n %f %f',1);
    Ainfo{i}=riga;
    tline = fgetl(fid); %go to next line
end 

fclose('all');

for i=1:Natoms
    MATRICE(i,5)=Ainfo{i}{2};
    MATRICE(i,6)=Ainfo{i}{3};
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen(ITPFILE,'r');

for i=1:Ntypes
    tline = fgetl(fid); %go to next line
    riga=textscan(fid,'%s %s %n %f %f %s %f %f',1);
    LJinfo{i}=riga;
end 

fclose('all');
            
%%%%%%%%%%%%%%%%%%Find oxygen%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:Ntypes  
    if strcmp('OW',LJinfo{j}{2})
        C6oxy = 4*LJinfo{j}{8}*LJinfo{j}{7}^6;
        C12oxy = 4*LJinfo{j}{8}*LJinfo{j}{7}^12;
    end     
end
%%%%%%%%%%%%%%%%%%Find oxygen%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:Natoms
    ii=0;

    for j=1:Ntypes  
        if strcmp(TYPES{i},LJinfo{j}{1})
            ii=j;
        end
    end
    
    %OPLS use geometic comb. rule; and sigma,epsilon
    MATRICE(i,2)=4*LJinfo{ii}{8}*LJinfo{ii}{7}^6; % sigma = LJinfo{ii}{7}
    MATRICE(i,3)=4*LJinfo{ii}{8}*LJinfo{ii}{7}^12; % epsilonLJinfo{ii}{8}
end
