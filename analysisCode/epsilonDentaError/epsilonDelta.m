%%-------------------------------------------------------------------------
% Copyright (C)  2013 Eliodoro Chiavazzo & Pietro Asinari
%
% This file is part of DeltaSasComputation package.

% Please remember to cite:
% 1. Chiavazzo E., Fasano M., Asinari P. and Decuzzi P., Scaling Behavior for the Water Transport in Nanoconfined Geometries, Nature Communications, 2014; DOI: http://dx.doi.org/10.1038/ncomms4565
% 2. Salassi, A. Cardellini, P. Asinari, R. Ferrando and G. Rossi, Water dynamics affects thermal transport at the surface of hydrophobic and hydrophilic irradiated nanoparticles, Nanoscale Adv., 2020, DOI: https://doi.org/10.1039/D0NA00094A

% DeltaSasComputation package is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% DeltaSasComputation package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with DeltaSasComputation package.  If not, see <http://www.gnu.org/licenses/>.
%%-------------------------------------------------------------------------


%%%%%%%%%%%%%%%%%%%%%%% DeltaSasComputation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This package is intended to be used for evaluating:
% 
% 1) characteristic length of nanoconfinement [nm],
% 2) solvent accessible surface [nm^2],
% 3) mean attractive potential well (according to 12,6 Lennard Jones potential) of solid-liquid non-bonded interactions [kJ/mol]
% 
% in geometries where water is nanoconfined, according to the method explained in:
% 
% Chiavazzo E., Fasano M., Asinari P. and Decuzzi P., Scaling Behavior for the Water Transport in Nanoconfined Geometries, Nature Communications, (2014)
% 
% The following four input files are needed to use DeltaSasComputation:
% 
% 1) topfile.top  (Topology)
% 2) itpfile.itp  (non-bonded force-field)
% 3) xvgfile.xvg  (SAS)
% 4) connolly.pdb (Geometry with Connolly surface)

%%%%%%%%%%%%%%%%%%%%%%% DeltaSasComputation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clear all
clc

%%-------------------------LIST OF CASES-------------------------
PARTICLE{1} = 'XXX'; % PARTICLE{i} is the name of the folder containing the input files for the i-th case
%%-------------------------LIST OF CASES-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%INPUT DATA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kq = 1/(1.23799e-20);              % 1/(4*pi*eps_0)^2									[C^-4*m^2*J^2]	    
muW = 7.50e-30;                    % water dipole										[C*m]   
electron = 1.602176565e-19;        % electron charge									[C]   
kB = 1.38*1e-23;                   % Boltzmann constant									[J/K]    
T = 300;                           % Room temperature									[K]
NA = 6.02e+23;                     % Avogadro number									[mol^-1] 
radius_lj=10.0;                     % cut-off radius										[Ang]
coeff=0.25;                        % How many kT?										[-] 
lambda=0.3:0.07:15;                % parametrization of line l							[-] 
LJmixing=2;                        % Mixing rule for Lennard-Jones parameters: 1-> Lorentz-Berthelot; 2-> Geometric mean 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%INPUT DATA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
load EPSrel % EPSrel contains an estimate of the relative permittivity of water as a function of distance from the charged surface (see e.g. E. Gongadze et. al, Electrochimica Acta 2013)
%
DATE=datestr(now,30);            % Current date
filename=strcat(PARTICLE{1},'.txt');    % The output file name is the current date
%
fid=fopen(filename,'wt');
fprintf(fid,'%s','*********************************');
fprintf(fid,'\n');
for i=1:max(size(PARTICLE))
    fid=fopen(filename,'a');
    text=strcat('(',num2str(i),')',' ',PARTICLE{i});
    fprintf(fid,'%s',text);
    fprintf(fid,'\n');
end
fprintf(fid,'%s','*********************************');
fprintf(fid,'\n');
text=strcat('Temperature:',' ',num2str(T),' ','[K]');
fprintf(fid,'%s',text);
fprintf(fid,'\n');
text=strcat('Kinetic energy:',' ',num2str(coeff),' ','*kB*T');
fprintf(fid,'%s',text);
fprintf(fid,'\n');
fprintf(fid,'%s','*************************************************************');
fprintf(fid,'\n');
fprintf(fid,'%s','Mean delta [nm]      SAS [nm^2]      Mean epsilon [kJ/mol]');
fprintf(fid,'\n');
fprintf(fid,'%s','*************************************************************');
fprintf(fid,'\n');
fclose('all');
%%
%
kin=coeff*kB*T*NA*ones(1,max(size(lambda)))/1000; % kinetic energy [kJ/mol]
%
for p=1:max(size(PARTICLE))
    
    cd(PARTICLE{p}) 
    
    text=strcat('Start: ',PARTICLE{p},'...');
    disp(text)

    [MATRICE,Natoms,C6oxy,C12oxy]=collectdata;  % Collect data about all atoms of the particle (see the attached routine)     
    if LJmixing==1
        sigo=(C12oxy/C6oxy)^(1/6);				%  [nm]
        epso=C6oxy*C6oxy/(4*C12oxy);			%  [kJ/mol]    
    end

 %%%%%%%%%%%%%%%%%%%%%%OPEN AND READ THE PDB FILE CONTAINING THE CONNOLLY%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    PDBFILE='connolly.pdb';						% Connolly surface as provided by GROMACS by the command g_sas (see README.txt)

    fid = fopen(PDBFILE,'r');
 
    for i=1:4
        tline=fgets(fid);
    end
    
    contC=0;
    contA=0;
    stop=0;

    i=0;

while stop==0
    
    i=i+1;

    riga=textscan(fid,'%s %n %s %s %n %n %n %n %n %n',1);
    
    if strcmp(riga{1},'TER')
        stop=1;
    elseif strcmp(riga{3},'DOT')	% Find points on the Connolly
        contC=contC+1;
        CenterMAT(contC,1)=riga{6};  
        CenterMAT(contC,2)=riga{7};
        CenterMAT(contC,3)=riga{8};
    elseif (1-strcmp(riga{4},'SOL')) && (1-strcmp(riga{4},'NA')) && ...
            (1-strcmp(riga{4},'CL')) && (1-strcmp(riga{4},'SOLP'))% Find atoms of the solid structure
        contA=contA+1;
        AtomMAT(contA,1)=riga{6};
        AtomMAT(contA,2)=riga{7};
        AtomMAT(contA,3)=riga{8};
    end
    
    tline = fgetl(fid);				% go to next line of the file
    
end

fclose('all');
%%%%%%%%%%%%%%%%%%%%%%OPEN AND READ THE PDB FILE CONTAINING THE CONNOLLY%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

delta=zeros(1,Natoms);
Minepsilon=zeros(1,Natoms);

for n=1:Natoms % Run over all atoms of the structure
    text=strcat('Progress',' ','(',PARTICLE{p},')',' ',' [%]','...');    
    disp(text)
    disp(100*n/Natoms)
    
    a = [AtomMAT(n,1);AtomMAT(n,2);AtomMAT(n,3)];% n-th atom from the structure
    NNC = nearestneighbour(a,CenterMAT','NumberOfNeighbours',1);        % Among points forming the Connolly, find the closest neighbor of a (n-th atom in the structure)
    c = [CenterMAT(NNC(1),1);CenterMAT(NNC(1),2);CenterMAT(NNC(1),3)];  % Among points forming the Connolly, c is the closest point to a

    xx=a(1)-lambda*(a(1)-c(1));		% x coordinate of line l originating from the center of atom a and passing through c  [Ang]
    yy=a(2)-lambda*(a(2)-c(2));		% y coordinate of line l originating from the center of atom a and passing through c  [Ang]
    zz=a(3)-lambda*(a(3)-c(3));		% z coordinate of line l originating from the center of atom a and passing through c  [Ang]

    NNA = nearestneighbour(a, AtomMAT', 'Radius', radius_lj); % Among atoms from the structure find nearest neighbors within a fixed cut-off distance 'radius_lj'
    NNLJ=max(size(NNA));								   % Total number of neighbors within 'radius_lj' 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%BUILDING UP CHMAT (matrix of local charges)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    qtot=0;
    cont=0;
    stop=0;
    while cont<NNLJ && stop==0            % Search for local dipoles else consider all the charges within the cut-off distance
        cont=cont+1;
        qtot=sum(MATRICE(NNA(1:cont),4)); % total charge [C]
        if abs(qtot)<1e-10
            stop=1;
        end
    end

    CHMAT=[AtomMAT(NNA(1:cont),1),AtomMAT(NNA(1:cont),2),AtomMAT(NNA(1:cont),3),MATRICE(NNA(1:cont),4)];
    CHMAT(:,1:3)=CHMAT(:,1:3)*1e-10;	% [m]
    CHMAT(:,4)=CHMAT(:,4)*electron;		% [C]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%BUILDING UP CHMAT (matrix of local charges)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%BUILD UP THE LOCAL POTENTIAL DUE TO LJ AND CHARGES %%%%%%%%%%%%%%%%%%%%%%%%%%%
    Vtot=zeros(1,max(size(lambda)));    % initialize the total potential along the line l

    for j=1:NNLJ						% run over neighbors
        index=NNA(j);
    
        C6 = MATRICE(index,2);			% [kJ/mol*nm^6] : LJ attractive term
        C12= MATRICE(index,3);			% [kJ/mol*nm^12]: LJ repulsive term 
    
    %%%%%%%%LJ PARAMETERS: MIXING with OXYGEN/WATER%%%%%%%%%%%%%%%%%%
        if LJmixing==1
            if C6>0 && C12>0
                siga=(C12/C6)^(1/6);   % [nm] 
                epsa=C6*C6/(4*C12);    % [kJ/mol]
            else
                epsa=0;               % [nm] 
                siga=0;               % [kJ/mol]
            end
            sigeff=0.5*(siga+sigo);       % Do Lorentz-Berthelot mixing  [nm]
            epseff=sqrt(epsa*epso);       % Do Lorentz-Berthelot mixing  [kJ/mol]
            C6mix=4*epseff*sigeff^6;      % [kJ/mol*nm^6]
            C12mix=4*epseff*sigeff^12;    % [kJ/mol*nm^12]
        else
            C6mix=sqrt(C6*C6oxy);    % Do geometric mean mixing [kJ/mol*nm^6]
            C12mix=sqrt(C12*C12oxy); % Do geometric mean mixing [kJ/mol*nm^12]
        end
    %%%%%%%%LJ PARAMETERS: MIXING with OXYGEN/WATER%%%%%%%%%%%%%%%%%%
        
        for kk=1:max(size(lambda)) % Add LJ contribution
             % distance of the j-th neighbor from a generic point of the line l [nm]
            rho=0.1*norm([AtomMAT(index,1),AtomMAT(index,2),AtomMAT(index,3)]-[xx(kk),yy(kk),zz(kk)]);
            Vtot(kk) = Vtot(kk) + C12mix/rho^12 - C6mix/rho^6;       % [kJ/mol]
        end
    end
    
    if sum(abs(CHMAT(:,4))>0) % If there are local charges to consider, add electrostatics
        for kk=1:max(size(lambda)) 
            r=1e-10*[xx(kk),yy(kk),zz(kk)];                           % Position where to compute the electric field [m]
            rho=0.1*norm([xx(1),yy(1),zz(1)]-[xx(kk),yy(kk),zz(kk)]); % Distance from atom a [nm]
            epsr=interp1(disnm,RelPer,rho,'linear','extrap');         % Relative permittivity of water (E. Gongadze et. al, Electrochimica Acta 2013 for sigma<0.2 C/m^2)
            VmeanC=ChargesPot(CHMAT,r,T,epsr);                        % Termally averaged potential energy [kJ/mol]       
            Vtot(kk) = Vtot(kk) + VmeanC;                             % [kJ/mol]   
        end
    end
    
% sigmas(n)=1e18*sum(CHMAT(:,4))/sum(MATRICE(NNA(1:cont),5));     % Estimate the local charge densities [C*m^-2]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%BUILD UP THE LOCAL POTENTIAL DUE TO LJ AND CHARGES %%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%COMPUTE DELTA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [XI,YI] = polyxpoly(0.1*lambda*norm(c-a),Vtot,0.1*lambda*norm(c-a),-kin); % Find intersection between potential and kinetic energy 

    if max(size(XI))>1
        delta(n)=abs(XI(2)-XI(1));              % [nm]       compute the local characteristic lenght
        Minepsilon(n)=min(Vtot);                % [kJ/mol]   compute the local potential well 
    else
        delta(n)=0;                             % [nm]
    end

end

Stot=sum(MATRICE(:,5));                     % [nm^2]    Total SAS


deltamean=delta*MATRICE(:,5)/Stot;          % [nm]      Particle charcteristic lenght as weighted average of the atomic SAS
epsilonmean=Minepsilon*MATRICE(:,5)/Stot;   % [kJ/mol]  Local potential well as weighted average of the atomic SAS

sigmaStot=sqrt(sum(MATRICE(:,6).^2));
sigmaEpsilon =sqrt(( epsilonmean^2*sigmaStot^2 + (Minepsilon.^2 * MATRICE(:,6).^2) ))/Stot;
sigmaDelta =  sqrt(( deltamean^2  *sigmaStot^2 + (delta.^2      * MATRICE(:,6).^2) ))/Stot;


disp('Mean delta [nm]:')
disp(deltamean)
disp(sigmaDelta)

disp('Volume of Influence [nm^3]:')
disp(deltamean*Stot)

disp('Mean epsilon [kJ/mol]:')
disp(epsilonmean)
disp(sigmaEpsilon)

cd ..
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%COMPUTE DELTA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%SAVE DATA TO FILE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Saving...')

fid=fopen(filename,'a');
fprintf(fid,'%6f+-%6f %6f %6f+-%6f',deltamean,sigmaDelta,Stot,epsilonmean,sigmaEpsilon);
fprintf(fid,'\n');
fclose('all');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%SAVE DATA TO FILE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear MATRICE Natoms CenterMAT AtomMAT delta deltamean Stot Vtot Minepsilon epsilonmean

end
