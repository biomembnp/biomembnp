%%-------------------------------------------------------------------------
% Copyright (C)  2013 Eliodoro Chiavazzo & Pietro Asinari
%
% This file is part of DeltaSasComputation package.

% Please remember to cite:
% Chiavazzo E., Fasano M., Asinari P. and Decuzzi P., Scaling Behavior for the Water Transport in Nanoconfined Geometries, Nature Communications, (2014)

% DeltaSasComputation package is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% DeltaSasComputation package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with DeltaSasComputation package.  If not, see <http://www.gnu.org/licenses/>.
%%-------------------------------------------------------------------------

%%-------------------------------------------------------------------------
% This subroutine provides with the averaged electrical potential at 
% a desired position r 
%%-------------------------------------------------------------------------
%
%%-------------------------------------------------------------------------
% CHMAT -> [N x 4] array with distances in [m] and charges in [C]
% r     -> [1 x 3] array with distances in [m]
% eps   -> medium permittivity
%%-------------------------------------------------------------------------
%
function Vmean=ChargesPot(CHMAT,r,T,epsr)

f=8987551787.99791/epsr;        % [J*m*C^-2]
pw=7.50e-30;                    % [C*m] water dipole
kB = 1.38*1e-23;                % [J/K] Boltzmann constant    
NA = 6.02e+23;                  % [mol^-1] Avogadro number

kinE=kB*T;

[N,Nxxx]=size(CHMAT);

Efield=zeros(1,3);

for i=1:N
    
    diffx=(r(1)-CHMAT(i,1));
    diffy=(r(2)-CHMAT(i,2));
    diffz=(r(3)-CHMAT(i,3));

    DEN=(diffx*diffx+diffy*diffy+diffz*diffz)^1.5;

    Efield(1)=Efield(1)+CHMAT(i,4)*diffx/DEN;
    Efield(2)=Efield(2)+CHMAT(i,4)*diffy/DEN;
    Efield(3)=Efield(3)+CHMAT(i,4)*diffz/DEN;

end

Efield=Efield*f;   % [V m^-1]
E=norm(Efield);    % [V m^-1]

NUM=pw*E;          % [J]

argument=NUM/kinE; % [-]

L=coth(argument)-1/argument;      % Langevin function [-]

Vmean=-1*NUM*L*NA/1000;           % [kJ/mol]