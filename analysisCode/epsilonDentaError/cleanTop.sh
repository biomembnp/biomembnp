#!/bin/bash

flag=0
while IFS='' read -r line || [[ -n "$line" ]]; do
	if [[ $line == *"moleculetype"* ]]; then
		flag=1
	fi

	if [ $flag == 1 ]; then
		if [[ ! $line == ";"* ]]; then
			echo "$line"
		fi
	fi

	if [[ $line == *"bonds"* ]]; then
		exit
	fi
done < "$1"
