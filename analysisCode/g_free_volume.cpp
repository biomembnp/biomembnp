/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

#include <string>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>



#include <gromacs/trajectoryanalysis.h>
#include <gromacs/pbcutil/pbc.h>

using namespace gmx;
using namespace std;


string int_to_string(int number){

ostringstream stream1;
stream1 << number;
return stream1.str();

}


/*! \brief
 * Template class to serve as a basis for user analysis tools.
 */
class AnalysisTemplate : public TrajectoryAnalysisModule
{
    public:
        AnalysisTemplate();

        virtual void initOptions(Options                    *options,
                                 TrajectoryAnalysisSettings *settings);
        virtual void initAnalysis(const TrajectoryAnalysisSettings &settings,
                                  const TopologyInformation        &top);

        virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                                  TrajectoryAnalysisModuleData *pdata);

        virtual void finishAnalysis(int nframes);
        virtual void writeOutput();

    private:
        class ModuleData;

        std::string                      base_filename;
        double                           cutoff;
        double                           x_low, x_high, y_low, y_high, z_low, z_high ;
        double                           cell_size ;
        Selection                        sel_;
        Selection                        center_;

        AnalysisNeighborhood             nb_;

        AnalysisData                     data_;
        AnalysisDataAverageModulePointer avem_;
};


AnalysisTemplate::AnalysisTemplate()
    : TrajectoryAnalysisModule("template", "Template analysis tool")
{
    registerAnalysisDataset(&data_, "avedist");
}


void
AnalysisTemplate::initOptions(Options                    *options,
                              TrajectoryAnalysisSettings *settings)
{
    static const char *const desc[] = {
        "This is a template for writing your own analysis tools for",
        "GROMACS. The advantage of using GROMACS for this is that you",
        "have access to all information in the topology, and your",
        "program will be able to handle all types of coordinates and",
        "trajectory files supported by GROMACS. In addition,",
        "you get a lot of functionality for free from the trajectory",
        "analysis library, including support for flexible dynamic",
        "selections. Go ahead an try it![PAR]",
        "To get started with implementing your own analysis program,",
        "follow the instructions in the README file provided.",
        "This template implements a simple analysis programs that calculates",
        "average distances from a reference group to one or more",
        "analysis groups."
    };

    options -> setDescription(desc);

    options -> addOption(FileNameOption("o")
                           .filetype(eftUnknown).outputFile()
                           .store(&base_filename).defaultBasename("grid_values")
                           .description("Grid values output."));

    options -> addOption(SelectionOption("selection")
                           .store(&sel_).required()
                           .description("Group to calculate distances from."));

    options -> addOption(SelectionOption("center")
                           .store(&center_).required()
                           .description("Group to calculate distances from."));


    options -> addOption(DoubleOption("x_low")
                    .store(&x_low)
                    .defaultValue(-1)
                    .description("Grid coordinate x lower limit"));

    options -> addOption(DoubleOption("x_high")
                    .store(&x_high)
                    .defaultValue(-1)
                    .description("Grid coordinate x upper limit"));


    options -> addOption(DoubleOption("y_low")
                    .store(&y_low)
                    .defaultValue(-1)
                    .description("Grid coordinate y lower limit"));

    options -> addOption(DoubleOption("y_high")
                    .store(&y_high)
                    .defaultValue(-1)
                    .description("Grid coordinate y upper limit"));


    options -> addOption(DoubleOption("z_low")
                    .store(&z_low)
                    .defaultValue(-1)
                    .description("Grid coordinate z lower limit"));

    options -> addOption(DoubleOption("z_high")
                    .store(&z_high)
                    .defaultValue(-1)
                    .description("Grid coordinate z upper limit"));

    options -> addOption(DoubleOption("cutoff")
                         .required()
                         .store(&cutoff)
                         .description("Distance threshold for empty space."));

    options -> addOption(DoubleOption("cell_size")
                         .required()
                         .store(&cell_size)
                         .description("Grid cell size"));


    settings -> setFlag(TrajectoryAnalysisSettings::efRequireTop);
}


void
AnalysisTemplate::initAnalysis(const TrajectoryAnalysisSettings &settings,
                               const TopologyInformation         & /*top*/)
{

    /*
    //nb_.setCutoff(cutoff_);

    //data_.setColumnCount(0, sel_.size());

    avem_.reset(new AnalysisDataAverageModule());
    data_.addModule(avem_);



    if (!fnDist_.empty())
    {
        AnalysisDataPlotModulePointer plotm(
                new AnalysisDataPlotModule(settings.plotSettings()));
        plotm->setFileName(fnDist_);
        plotm->setTitle("Average distance");
        plotm->setXAxisIsTime();
        plotm->setYLabel("Distance (nm)");
        data_.addModule(plotm);
    }
    */

}


void
AnalysisTemplate::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                               TrajectoryAnalysisModuleData *pdata)
{
    //AnalysisDataHandle         dh     = pdata->dataHandle(data_);
    const Selection            &sel   = pdata->parallelSelection(sel_);


    //dh.startFrame(frnr, fr.time);


    //extract (cubic) box dimensions
    vector <double> box(3);
    for (int i = 0; i < 3; ++i) box.at(i)=pbc->box[i][i];

    //calculate grid boundaries and ticks, according to box size which can be overridden by boundary parameters
    if(x_low  <0)  x_low=0;
    if(x_high <0)  x_high=box.at(0);
    if(y_low  <0)  y_low=0;
    if(y_high <0)  y_high=box.at(1);
    if(z_low  <0)  z_low=0;
    if(z_high <0)  z_high=box.at(2);
    vector <double> xticks, yticks, zticks;
    double x_value = x_low;
    double y_value = y_low;
    double z_value = z_low;
    while(x_value<x_high) {xticks.push_back(x_value); x_value += cell_size; }
    while(y_value<y_high) {yticks.push_back(y_value); y_value += cell_size; }
    while(z_value<z_high) {zticks.push_back(z_value); z_value += cell_size; }

    //initialize grid for data analysis
    vector < vector < vector  <int>   > >   grid ( xticks.size() , vector< vector <int> > ( yticks.size(), vector <int> (zticks.size(), 0) ) );

    //calculate grid values, looping on every selection bead

    int n_sel_beads = sel.posCount();

    for(int bead = 0; bead< n_sel_beads ; bead ++){

       vector <double> bead_pos;
       for(int i = 0; i < 3; i++)   bead_pos.push_back(sel.position(bead).x()[i]);

       //extract ticks subvectors (to significantly reduce grid search time)
       vector <double> sub_xticks, sub_yticks, sub_zticks;
       int shift_index_x, shift_index_y, shift_index_z;
       for(int i = 0; i < xticks.size(); i++ ) if( fabs(xticks.at(i)-bead_pos.at(0)) < cutoff ) {sub_xticks.push_back(xticks.at(i)); shift_index_x = i ; };
       for(int i = 0; i < yticks.size(); i++ ) if( fabs(yticks.at(i)-bead_pos.at(1)) < cutoff ) {sub_yticks.push_back(yticks.at(i)); shift_index_y = i ; };
       for(int i = 0; i < zticks.size(); i++ ) if( fabs(zticks.at(i)-bead_pos.at(2)) < cutoff ) {sub_zticks.push_back(zticks.at(i)); shift_index_z = i ; };

        for(int i=0; i< sub_xticks.size(); i++) {
              for(int j=0; j< sub_yticks.size(); j++){
                 for(int k=0; k< sub_zticks.size(); k++){

                        double dist_x= sub_xticks.at(i) - bead_pos.at(0);
                        double dist_y= sub_yticks.at(j) - bead_pos.at(1);
                        double dist_z= sub_zticks.at(k) - bead_pos.at(2);

                        if( dist_x*dist_x + dist_y*dist_y + dist_z*dist_z < cutoff*cutoff) {
                            grid.at(shift_index_x - sub_xticks.size() + i + 1 )
                                .at(shift_index_y - sub_yticks.size() + j + 1 )
                                .at(shift_index_z - sub_zticks.size() + k + 1 ) = 1;

                                //diagnostics: check indexes!

                        }

                 }
              }
        }
    }

        //print the grid in xyz format into file




      /*
        string filename = base_filename + int_to_string(fr.time) + "ps.xyz" ;   //initialize filename for frame data output
        ofstream grid_file(filename.c_str());

        grid_file << xticks.size() * yticks.size() * zticks.size() << endl;
        grid_file << "Free volume grid at time " << fr.time << " ps" << endl;



       for(int i=0; i< xticks.size(); i++) {
            for(int j=0; j< yticks.size(); j++){
                for(int k=0; k< zticks.size(); k++){

                    if (grid.at(i).at(j).at(k) == 0 ) grid_file << "EMPTY"; else grid_file << "FULL";

                    grid_file << "    " << xticks.at(i)*10;
                    grid_file << "    " << yticks.at(j)*10;
                    grid_file << "    " << zticks.at(k)*10;

                    grid_file << endl;


                }
            }
       }

       */


       // calculate (geometric) center position

        int n_center_beads = sel.posCount();
        vector <float> center_pos(3,0);
        for(int bead = 0; bead < n_center_beads ; bead ++){
            for(int i = 0; i < 3; i++)   center_pos.at(i) += sel.position(bead).x()[i];
        }
        for(int i = 0; i < 3; i++)   center_pos.at(i) /=  (double) n_center_beads;



       // print the grid in gro format into file




/*
	int n_atoms=atoms.size();

	fprintf(output,"%s MODIFIED\n",first_line.c_str());
	fprintf(output,"%d\n",n_atoms);
	for (int i = 0; i < n_atoms; ++i) fprintf(output,"%5d%-5s%5s%5d%8.3f%8.3f%8.3f%8.4f%8.4f%8.4f\n",atoms[i].residue_number,atoms[i].residue_name.c_str(),atoms[i].atom_name.c_str(),atoms[i].atom_number,atoms[i].x,atoms[i].y,atoms[i].z,atoms[i].vel_x,atoms[i].vel_y,atoms[i].vel_z);
	fprintf(output,"%s\n",last_line.c_str());

	fclose(output);

	cout<<"File "<<(filename+".gro").c_str()<<" created."<<endl;
*/




        string filename = base_filename + int_to_string(fr.time) + "ps.gro" ;   //initialize filename for frame data output
        FILE *grid_file;
	    grid_file= fopen(filename.c_str(), "w");


        fprintf(grid_file, "Free volume grid at time (ps) t= %f \n", fr.time);
        fprintf(grid_file, "%d\n",  (int) (xticks.size() * yticks.size() * zticks.size() + 1) ); // + 1 is for center atom

        int residue_number=1;
        string residue_name= "POINT";
        float unit_factor=1;  //to switch between angstroms and nanometers
        string atom_name;

        // insert grid points atoms

        vector <int> full_points, empty_points;

        int atom_index=1;
        int atom_number_limit = 100000;

        for(int i=0; i< xticks.size(); i++) {
                for(int j=0; j< yticks.size(); j++){
                    for(int k=0; k< zticks.size(); k++){

                        if (grid.at(i).at(j).at(k) == 0 ) { atom_name = "EMPTY"; empty_points.push_back(atom_index) ; } else { atom_name = "FULL"; full_points.push_back(atom_index) ; }
                        fprintf(grid_file,"%5d%-5s%5s%5d%8.3f%8.3f%8.3f\n",residue_number,residue_name.c_str(), atom_name.c_str(), atom_index%atom_number_limit, (double)(xticks.at(i)*unit_factor), (double)(yticks.at(j)*unit_factor), (double)(zticks.at(k)*unit_factor));
                        atom_index++;
                    }
                }
           }

        // insert center atom
        atom_name = "CENTR";
        int center_point= atom_index;
        fprintf(grid_file, "%5d%-5s%5s%5d%8.3f%8.3f%8.3f\n", residue_number, residue_name.c_str(), atom_name.c_str(), atom_index%atom_number_limit, (double)center_pos.at(0), (double)center_pos.at(1), (double)center_pos.at(2) );
        fprintf(grid_file, "%f  %f  %f\n", box.at(0) * unit_factor, box.at(1) * unit_factor, box.at(2) * unit_factor);
        fclose(grid_file);

        // print index file
        string filename_index = base_filename + int_to_string(fr.time) + "ps.ndx" ;   //initialize filename for frame data output
        ofstream index_file(filename_index.c_str());

        index_file << "[ EMPTY ]" << endl;
        for(int i=0; i < empty_points.size(); i++ )  {
                index_file << empty_points.at(i) << " \t";
                if(i%10==9) index_file<<endl;
        }

        index_file << endl;

        index_file << "[ FULL ]" << endl;
        for(int i=0; i < full_points.size(); i++ )   {
                index_file << full_points.at(i) << " \t";
                if(i%10==9) index_file<<endl;
        }

        index_file << endl;

        index_file << "[ CENTER ]" << endl;
        index_file << center_point ;
        index_file.close();

        //dh.finishFrame();
}


void
AnalysisTemplate::finishAnalysis(int /*nframes*/)
{
}


void
AnalysisTemplate::writeOutput()
{

}

/*! \brief
 * The main function for the analysis template.
 */
int
main(int argc, char *argv[])
{
    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<AnalysisTemplate>(argc, argv);
}