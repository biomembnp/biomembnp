#include "xvgPlot.hpp"

void xvgPlot::addColumn(const string& title)
{
	data.push_back(std::vector<double>());
	column_titles.push_back(title);
}

void xvgPlot::plot(const string& filename)
{
	if(ifstream(filename.c_str()).good()) //file already exist
	{
		size_t i = 1;
		string back = "";
		do
		{
			back = "#"+filename+"."+to_string(i++)+"#";
		} while(ifstream(back.c_str()).good());
		std::rename(filename.c_str(), back.c_str());
	}

	//verify all columns have the same number of elements:
	const size_t n_columns = data.size();
	const size_t n_rows = data[0].size();

	for(size_t i = 1; i < n_columns; ++i)
	{
		if(data[i].size() != n_rows)
		{
			throw std::runtime_error("xvgPlot: Error! columns have different number of elements.");
		}
	}

	std::chrono::system_clock::time_point time = std::chrono::system_clock::now();
    std::time_t t = std::chrono::system_clock::to_time_t(time);

	ofstream output_file(filename.c_str());
	if(!output_file)
	{
		throw std::runtime_error(filename + ": no such file!");
	}

	output_file<<"# This file was created on " + string(std::ctime(&t))<<endl;
    output_file<<headerString<<endl<<endl;
    output_file<<"# Command line: "<<commandLine<<endl<<endl;

	for(auto it : comments)
	{
		output_file<<"# "<<it<<endl;
	}

	for(auto it(column_titles.begin()); it != column_titles.end(); ++it)
	{
		output_file<<"# @"<<distance(column_titles.begin(), it)<<": "<<*it<<endl;
	}
	
	for(size_t i = 0; i < n_rows; ++i)
	{
		output_file<<fixed<<setprecision(2)<<setw(5)<<data[0][i];
		for (size_t j = 1; j < n_columns; ++j)
		{
			output_file<<" "<<fixed<<setprecision(dataPrecision)<<setw(dataLength)<<data[j][i];
		}
		
		output_file<<endl;
	}

	output_file.close();
}
