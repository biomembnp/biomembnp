/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <numeric>
#include <algorithm>

#include <gromacs/version.h>
#include <gromacs/trajectoryanalysis.h>
#include <gromacs/pbcutil/pbc.h>
#include "statistics.h"

#include "xvgPlot.hpp"

using namespace gmx;
using namespace std;

static const char *const description[] = 
    {
        ""    
    };

static string commandLineString = "";

static const string plotHeaderString = "";
    //"# ***************************************\n"\
    //"# *      msd of confined molecules      *\n"\
    //"# *           Sebastian Salassi         *\n"\
    //"# ***************************************\n\n"\
    //"# Please cite:\n"\
    //"# "Water dynamics affects thermal transport at the surface of hydrophobic and hydrophilic irradiated nanoparticles",\n"\
    //"# S. Salassi, A. Cardellini, P. Asinari, R. Ferrando and G. Rossi\n"\
    //"# Nanoscale Adv., 2020\n"\
    //"# DOI: https://doi.org/10.1039/D0NA00094A";

class msdSurfaceTool : public TrajectoryAnalysisModule
{
public:
    msdSurfaceTool();

#if (GMX_API_VERSION < 20160000)
    virtual void initOptions(Options *options, TrajectoryAnalysisSettings *settings);
#else
    virtual void initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings);
#endif
    virtual void optionsFinished(TrajectoryAnalysisSettings *settings);
    virtual void initAfterFirstFrame(const TrajectoryAnalysisSettings &settings, const t_trxframe &fr);
    virtual void initAnalysis(const TrajectoryAnalysisSettings &settings, const TopologyInformation &top);

    virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata);

    virtual void finishAnalysis(int nframes);
    virtual void writeOutput();

private:
    string outputFilename;
    
    Selection refsel_;
    Selection sel_;

    float cutoff, tdelta, timeStep;
    float beginFit, endFit;
    float totalTime;

    AnalysisNeighborhood nb;

    vector<RVec> referencePositions;
    vector<int> referenceIndex;
    vector<vector<double>> msd;
};

msdSurfaceTool::msdSurfaceTool(): totalTime(0)
#if (GMX_API_VERSION < 20160000)
    , TrajectoryAnalysisModule("g_msdSurface", "")
#endif
   
{

}

#if (GMX_API_VERSION < 20160000)
void msdSurfaceTool::initOptions(Options *options, TrajectoryAnalysisSettings *settings)
#else
void msdSurfaceTool::initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings)
#endif
{
#if (GMX_API_VERSION < 20160000)
    options->setDescription(description);
#else
    settings->setHelpText(description);
#endif

    settings->setPBC(true);
    settings->setRmPBC(false);
    settings->setFlag(TrajectoryAnalysisSettings::efRequireTop | 
                      TrajectoryAnalysisSettings::efNoUserPBC  |
                      TrajectoryAnalysisSettings::efNoUserRmPBC );

    options->addOption(FileNameOption("o").outputFile().filetype(eftGenericData)
             .store(&outputFilename).defaultBasename("msd").required()
             .description("Mean square displacement plot"));
    
    options->addOption(FloatOption("trestart").store(&tdelta).defaultValue(0.).required()
             .description("Extract an MSD every 'trestart' to average it. (0 means whole trajectory) (ps)"));
    
    options->addOption(FloatOption("d").store(&cutoff).defaultValue(1).required()
             .description("Cutoff distance to get solvent atoms from the reference group. (nm)"));
    
    options->addOption(SelectionOption("reference").store(&refsel_).onlyAtoms()
             .description("Reference group").required());
    
    options->addOption(SelectionOption("select").required().store(&sel_).required()
             .description("Group to calculate MSD for"));

    options->addOption(FloatOption("beginfit").store(&beginFit).defaultValue(0)
    		 .description("Time to start fit, if '0' get 10 \\% of trajectory. (ps)"));

    options->addOption(FloatOption("endfit").store(&endFit).defaultValue(0)
    		 .description("Time to end fit, if '0' get 90 \\% of trajectory. (ps)"));
}

void msdSurfaceTool::optionsFinished(TrajectoryAnalysisSettings *settings)
{
    if(cutoff <= 0)
    {
        GMX_THROW(InconsistentInputError("The cutoff value cannot be zero or negative."));
    }

    if(tdelta < 0)
    {
        GMX_THROW(InconsistentInputError("The time interval cannot be negative."));
    }
}

void msdSurfaceTool::initAnalysis(const TrajectoryAnalysisSettings &settings, const TopologyInformation &top)
{
    nb.setCutoff(cutoff);
}

void msdSurfaceTool::initAfterFirstFrame(const TrajectoryAnalysisSettings &settings, const t_trxframe &fr)
{
   timeStep = fr.time;
}

void msdSurfaceTool::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata)
{
    // Convert tdelta from ps to number of frame
    // For the second frame fr.time corresponds to the time step of the trajectory
    if(frnr == 1)
    {   
        timeStep = fr.time - timeStep;
        if(tdelta > 0 && tdelta <= timeStep)
        {
            GMX_THROW(InconsistentInputError("Time interval cannot be less than trajectory time step."));
        }
        tdelta = (int)(tdelta / timeStep);
    }

    const Selection &sel    = pdata->parallelSelection(sel_);

    //Get the reference position every tdelta
    if(frnr == 0 || ( tdelta > 0 && (frnr % static_cast<int>(tdelta)) == 0 ) )
    {
        const Selection &refsel = pdata->parallelSelection(refsel_);

        referencePositions.clear();
        referenceIndex.clear();

        AnalysisNeighborhoodSearch nbsearch = nb.initSearch(pbc, refsel);
        AnalysisNeighborhoodPairSearch pairSearch = nbsearch.startPairSearch(sel);
        AnalysisNeighborhoodPair pair;
        while (pairSearch.findNextPair(&pair))
        {
            referenceIndex.push_back(pair.testIndex());
            referencePositions.push_back( sel.position(pair.testIndex()).x() );
        }

        //add a new vector
        vector<double> v;
        if(!msd.empty())
        {
            v.reserve(msd.back().size());
        }       
        msd.push_back(v);

    } else
    {
        size_t nSel = referenceIndex.size();
        double currentMsd = 0;
        for(size_t i = 0; i < nSel; ++i)
        {
            rvec dx;
            pbc_dx(pbc, sel.position(referenceIndex[i]).x(), referencePositions[i], dx);

            currentMsd += dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2];
        }

        msd.back().push_back(currentMsd/nSel);
    }

    totalTime = fr.time;
}

void msdSurfaceTool::finishAnalysis(int nframes)
{
	if(beginFit <= 0)
	{
		beginFit = 0.1*totalTime;
	}
	if(endFit <= 0)
	{
		endFit = 0.9*totalTime;
	}

    size_t maxSize = 0;

    for(auto &it : msd)
    {
        maxSize = std::max(maxSize, it.size());
    }

    xvgPlot graph("", commandLineString);
    graph.addComment("Reference group: " + string(refsel_.name()));
    graph.addComment("Selection group: " + string(sel_.name()));
    graph.addComment("Cutoff: " + std::to_string(cutoff));
    if(tdelta > 0)
    { 
        graph.addComment("TDelta: " + std::to_string(tdelta*timeStep));
    } else
    {
        graph.addComment("TDelta: whole trajectory");
    }

    graph.addColumn("Time [ps]");
    graph.addColumn("MSD [nm^2]");

    gmx_stats_t msdStats;
    msdStats = gmx_stats_init();

    for(size_t i = 0; i < maxSize; ++i)
    {
        double sum = 0;
        int count = 0;
        for(auto &it : msd)
        {
            try
            {
                sum += it.at(i); //.at needed for exception
                count++;
            } catch(std::out_of_range &e)
            {
                break;
            }
        }
        const double time = i*timeStep;
        const double average = sum/count;
        if(time >= beginFit && time <= endFit)
        {
        	gmx_stats_add_point(msdStats, time, average, 0, 0);
        }
        graph.addPoint(0, time);
        graph.addPoint(1, average);
    }

    //y = a*x + b
    real a = 0, da;
    gmx_stats_get_ab(msdStats, elsqWEIGHT_NONE, &a, NULL, &da, NULL, NULL, NULL);

    float diff = a*1000./6., ddiff = da*1000./6.;
    string diffusion = "D = " + std::to_string(diff) + " +- " + std::to_string(ddiff) + " e-5 cm^2/s";
    cout<<"\nFit from "<<beginFit<<" ps and "<<endFit<<" ps.\n";
    cout<<diffusion<<endl;

    graph.addComment("Fit from "+std::to_string(beginFit)+" ps and "+std::to_string(endFit)+" ps.");
    graph.addComment(diffusion);
    graph.plot(outputFilename);
}

void msdSurfaceTool::writeOutput()
{

}


/*! \brief
 * The main function for the analysis template.
 */
int main(int argc, char *argv[])
{
	for(int i = 0; i < argc; ++i)
	{
		commandLineString+= argv[i];
		commandLineString+= " ";
	}

    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<msdSurfaceTool>(argc, argv);
}
