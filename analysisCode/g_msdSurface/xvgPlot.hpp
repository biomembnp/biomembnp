#ifndef _XVG_PLOT_HPP_
#define _XVG_PLOT_HPP_

#include <vector>
#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <chrono>

using namespace std;

#ifndef PRECISION
#define PRECISION 4 //default precision
#endif

static constexpr int dataPrecision = PRECISION;
static constexpr int dataLength = PRECISION + 5;

class xvgPlot
{
public:
	xvgPlot(const string& header, const string& cmdLine = ""): headerString(header), commandLine(cmdLine)
	{

	}
	xvgPlot(const xvgPlot& ) = delete;
	xvgPlot(xvgPlot&& ) = delete;

	xvgPlot operator=(const xvgPlot& ) = delete;
	xvgPlot operator=(xvgPlot&& ) = delete;

	void addColumn(const string& );

	inline void addPoint(const size_t& i, const double& value) 
	{ 
		data[i].push_back(value);
	}
	
	inline void addComment(const string& comment)
	{
		comments.push_back(comment);
	}

	inline void setCommandLine(const string& cmdLine)
	{
		commandLine = commandLine;
	}

	void plot(const string& );

private:
	string headerString, commandLine;

	vector< vector<double> > data;
	list<string> comments, column_titles;
};

#endif // _XVG_PLOT_HPP_
