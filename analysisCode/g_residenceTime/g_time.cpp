/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

#include <omp.h>

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <numeric>
#include <algorithm>

#include <gromacs/version.h>
#include <gromacs/trajectoryanalysis.h>
#include <gromacs/pbcutil/pbc.h>
#include <gromacs/topology/mtop_util.h>
#include <gromacs/fileio/trxio.h>
#include "statistics.h"

#include "xvgPlot.hpp"

using namespace gmx;
using namespace std;

static const char *const description[] = 
    {
        ""    
    };

static string commandLineString = "";

static const string plotHeaderString = "";
    //"# ***************************************\n"\
    //"# *    characteristic residence time    *\n"\
    //"# *         Sebastian Salassi           *\n"\
    //"# ***************************************\n\n"\
    //"# Please cite:\n"\
    //"# "Water dynamics affects thermal transport at the surface of hydrophobic and hydrophilic irradiated nanoparticles",\n"\
    //"# S. Salassi, A. Cardellini, P. Asinari, R. Ferrando and G. Rossi\n"\
    //"# Nanoscale Adv., 2020\n"\
    //"# DOI: https://doi.org/10.1039/D0NA00094A";

struct kineticEnergy
{
	kineticEnergy(): kin(0), kout(0), tau(0) {}
	kineticEnergy(const float a, const float b, const float c): kin(a), kout(b), tau(c) {}
	
	float kin, kout;
	float tau;
};

class residenceTimeTool : public TrajectoryAnalysisModule
{
public:
    residenceTimeTool();

#if (GMX_API_VERSION < 20160000)
    virtual void initOptions(Options *options, TrajectoryAnalysisSettings *settings);
#else
    virtual void initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings);
#endif
    virtual void optionsFinished(TrajectoryAnalysisSettings *settings);
    virtual void initAfterFirstFrame(const TrajectoryAnalysisSettings &settings, const t_trxframe &fr);
    virtual void initAnalysis(const TrajectoryAnalysisSettings &settings, const TopologyInformation &top);

    virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata);

    virtual void finishAnalysis(int nframes);
    virtual void writeOutput();

private:
    string outputFilename, outputTauFile;
    
    Selection refsel_;
    Selection sel_;

    float cutoff, timeStep;
    float totalTime;
    float averageInside;

    AnalysisNeighborhood nb;

    //vector< BasicVector<float> > prev_velocities;
    vector<int> referenceIndex, prev_referenceIndex, inProgress, inside;
    map<int, kineticEnergy> currentEnergy;
    vector<kineticEnergy> kineticData;

    t_atoms atoms;
    int threads;
};

residenceTimeTool::residenceTimeTool(): totalTime(0), averageInside(0), threads(1)
#if (GMX_API_VERSION < 20160000)
    , TrajectoryAnalysisModule("g_msdSurface", "")
#endif
   
{

}

#if (GMX_API_VERSION < 20160000)
void residenceTimeTool::initOptions(Options *options, TrajectoryAnalysisSettings *settings)
#else
void residenceTimeTool::initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings)
#endif
{
#if (GMX_API_VERSION < 20160000)
    options->setDescription(description);
#else
    settings->setHelpText(description);
#endif

    settings->setFlags(TRX_READ_X | TRX_NEED_X); // | TRX_READ_V | TRX_NEED_V);
    settings->setFrameFlags(TRX_READ_X | TRX_NEED_X); // | TRX_READ_V | TRX_NEED_V);

    settings->setPBC(true);
    settings->setRmPBC(false);
    settings->setFlag(TrajectoryAnalysisSettings::efRequireTop | 
                      TrajectoryAnalysisSettings::efNoUserPBC  |
                      TrajectoryAnalysisSettings::efNoUserRmPBC );

    //options->addOption(FileNameOption("o").outputFile().filetype(eftPlot)
    //         .store(&outputFilename).defaultBasename("rawKineticData").required()
    //         .description("raw kinetic data"));

    options->addOption(FileNameOption("ot").outputFile().filetype(eftPlot)
             .store(&outputTauFile).defaultBasename("rawResidenceTime").required()
             .description("raw residence time data"));
    
    options->addOption(FloatOption("d").store(&cutoff).required()
             .description("Cutoff distance to get solvent atoms from the reference group. (nm)"));

    options->addOption(IntegerOption("nt").store(&threads).defaultValue(1)
             .description("Number of threds"));
    
    options->addOption(SelectionOption("reference").required().store(&refsel_).onlyAtoms()
    	     /*.evaluateVelocities()*/.onlyStatic().description("Reference group"));
    
    options->addOption(SelectionOption("select").required().store(&sel_).onlyAtoms()
    		 /*.evaluateVelocities()*/.onlyStatic().onlySortedAtoms()
             .description("Group to calculate kinetic energy for"));
}

void residenceTimeTool::optionsFinished(TrajectoryAnalysisSettings *settings)
{
    if(cutoff <= 0)
    {
        GMX_THROW(InconsistentInputError("The cutoff value cannot be zero or negative."));
    }

    omp_set_num_threads(threads);
}

void residenceTimeTool::initAnalysis(const TrajectoryAnalysisSettings &settings, const TopologyInformation &top)
{
	//sel_.setEvaluateVelocities(true);
    //refsel_.setEvaluateVelocities(true);

	if(top.hasTopology())
    {
    	atoms = gmx_mtop_global_atoms(top.mtop());
    	//idef = top.topology()->idef;
    } else
    {
    	GMX_THROW(InconsistentInputError("The option -s must be specified."));
    }

    nb.setCutoff(cutoff);
}

void residenceTimeTool::initAfterFirstFrame(const TrajectoryAnalysisSettings &settings, const t_trxframe &fr)
{
   timeStep = fr.time;
}

void residenceTimeTool::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata)
{
    // Convert tdelta from ps to number of frame
    // For the second frame fr.time corresponds to the time step of the trajectory
    if(frnr == 1)
    {   
        timeStep = fr.time - timeStep;
        //if(tdelta > 0 && tdelta <= timeStep)
        //{
        //    GMX_THROW(InconsistentInputError("Time interval cannot be less than trajectory time step."));
        //}
        //tdelta = (int)(tdelta / timeStep);
    }

    const Selection &sel    = pdata->parallelSelection(sel_);
    const Selection &refsel = pdata->parallelSelection(refsel_);
    //cout<<sel.posCount()<<endl<<refsel.posCount()<<endl;

    referenceIndex.reserve(sel.posCount()*2);
    
    if(threads == 1)
    {
    	AnalysisNeighborhoodSearch nbsearch = nb.initSearch(pbc, refsel);
    	AnalysisNeighborhoodPairSearch pairSearch = nbsearch.startPairSearch(sel);
    	AnalysisNeighborhoodPair pair;
    	while (pairSearch.findNextPair(&pair))
    	{
    		const int tIndex = pair.testIndex();
    		string atomName = string(*atoms.atomname[ sel.position(tIndex).atomIndices()[0] ]);
    		//cout<<atomName<<endl;
    		atomName.erase(std::remove(atomName.begin(), atomName.end(), ' '), atomName.end());
   			if(atomName == "OW")
   			{
    	    	referenceIndex.push_back(tIndex);
    	    }
    	}
    } else
    {
    	#pragma omp parallel for
    	for(int i = 0; i < refsel.posCount(); ++i)
		{
			AnalysisNeighborhoodSearch nbsearch = nb.initSearch(pbc, refsel.position(i).x());
			AnalysisNeighborhoodPairSearch pairSearch = nbsearch.startPairSearch(sel);
			AnalysisNeighborhoodPair pair;
			while (pairSearch.findNextPair(&pair))
			{
				const int tIndex = pair.testIndex();
    			string atomName = string(*atoms.atomname[ sel.position(tIndex).atomIndices()[0] ]);
    			atomName.erase(std::remove(atomName.begin(), atomName.end(), ' '), atomName.end());
   				if(atomName == "OW")
   				{
    	    		referenceIndex.push_back(tIndex);
    	    	}
			}
		}
    }

	referenceIndex.shrink_to_fit();     
    std::sort(referenceIndex.begin(), referenceIndex.end());
    referenceIndex.erase(std::unique(referenceIndex.begin(), referenceIndex.end()), referenceIndex.end());

	for(size_t i = 0; i < referenceIndex.size() && !prev_referenceIndex.empty() /*&& !prev_velocities.empty()*/; ++i)
	{
		auto itFind = std::find(prev_referenceIndex.begin(), prev_referenceIndex.end(), referenceIndex[i]);
		if( itFind == prev_referenceIndex.end()) // e' entrata
		{
			float kin = 0;
			//for(size_t j = 0; j < 3; ++j)
			//{
			//	const BasicVector<float> vel(prev_velocities.at(referenceIndex.at(i)+j));
			//	kin += 0.5*sel.position(referenceIndex.at(i)+j).mass()*(vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2]);
			//}
			currentEnergy[referenceIndex.at(i)] = kineticEnergy(kin, 0., timeStep);
			inProgress.push_back(referenceIndex.at(i));
			
			//cout<<endl<<referenceIndex[i]<<" ";
			//cout<<currentEnergy.at(referenceIndex.at(i)).kin<<endl;
		}
	}

	//cout<<endl;
	//for(auto it = inside.begin(); it != inside.end(); ++it)
	//{
	//	cout<<*it<<" ";
	//}
	//cout<<endl;

	for(size_t i = 0; i < inside.size() && !referenceIndex.empty(); ++i)
	{
		//if(inside[i] < 0)
		//{
		//	continue;
		//}
		bool isInside = false;
		for(size_t k = 0; k < referenceIndex.size(); ++k)
		{
			if(inside[i] == referenceIndex[k])
			{
				isInside = true;
				currentEnergy.at(inside[i]).tau += timeStep;
				break;
			}
		}

		if(!isInside) // e' uscita
		{
			float kout = 0;
			//for(size_t j = 0; j < 3; ++j)
			//{
				//const SelectionPosition p = sel.position(inside.at(i)+j);
				//kout += 0.5*p.mass()*(p.v()[0]*p.v()[0] + p.v()[1]*p.v()[1] + p.v()[2]*p.v()[2]);
			//	const BasicVector<float> vel(prev_velocities.at(inside.at(i)+j));
			//	kout += 0.5*sel.position(inside.at(i)+j).mass()*(vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2]);
			//}
			currentEnergy.at(inside[i]).kout = kout;
			kineticData.push_back(currentEnergy.at(inside[i]));
			
			currentEnergy.erase(currentEnergy.find(inside[i])); //togliamo la mol uscita
			inside[i] = -1; // e' uscita bisogna toglierla
		}
	}

	//Togliamo le mol uscite e aggiungiamo quelle nuove
	inside.erase(std::remove_if(inside.begin(), inside.end(), [](int i){ return i == -1; }), inside.end());
	inside.insert(inside.end(), inProgress.begin(), inProgress.end());
	inProgress.clear();

	averageInside+= referenceIndex.size();
    prev_referenceIndex = std::move(referenceIndex);
    referenceIndex.clear();

    //prev_velocities.clear();
    //prev_velocities.resize(sel.velocities().size());

    //#pragma omp parallel for
    // for( size_t i = 0; i < prev_velocities.size(); ++i)
    // {
    // 	prev_velocities[i][0] = sel.velocities()[i][0];
    // 	prev_velocities[i][1] = sel.velocities()[i][1];
    // 	prev_velocities[i][2] = sel.velocities()[i][2];
    // }

    totalTime = fr.time;
}

void residenceTimeTool::finishAnalysis(int nframes)
{
    // xvgPlot graph("", commandLineString);
    // graph.addComment("Reference group: " + string(refsel_.name()));
    // graph.addComment("Selection group: " + string(sel_.name()));
    // graph.addComment("Cutoff: " + std::to_string(cutoff));
    // graph.addColumn("Kin [kJ/mol]");
    // graph.addColumn("Kout [kJ/mol]");

    xvgPlot graphTau("", commandLineString);
    graphTau.addComment("Reference group: " + string(refsel_.name()));
    graphTau.addComment("Selection group: " + string(sel_.name()));
    graphTau.addComment("Cutoff: " + std::to_string(cutoff));
    graphTau.addColumn("Tau [ps]");

    gmx_stats_t tauStats; //, kinStats, koutStats, koutinStats;
    tauStats    = gmx_stats_init();
    // kinStats    = gmx_stats_init();
    // koutStats   = gmx_stats_init();
    // koutinStats = gmx_stats_init();
    
    for(auto it = kineticData.begin(); it < kineticData.end(); ++it)
    {
    	graphTau.addPoint(0, it->tau);

        // graph.addPoint(0, it->kin);
        // graph.addPoint(1, it->kout);

        gmx_stats_add_point(tauStats   , 0, it->tau,  0, 0);
        // gmx_stats_add_point(kinStats   , 0, it->kin,  0, 0);
        // gmx_stats_add_point(koutStats  , 0, it->kout, 0, 0);
        // gmx_stats_add_point(koutinStats, 0, (it->kout - it->kin), 0, 0);
    }
    // graph.plot(outputFilename);
    graphTau.plot(outputTauFile);

    //gmx_stats_make_histogram(tauStats, real binwidth, int *nbins, ehistoY, 0, real **x, real **y);

    cout<<"Average molecules inside: "<<averageInside/nframes<<endl;
    cout<<"   \tAverage\tDev. Std\tError Std."<<endl;

    float aver, sigma, error;
    gmx_stats_get_average( tauStats, &aver);
    gmx_stats_get_sigma(   tauStats, &sigma);
    gmx_stats_get_error(   tauStats, &error);
    cout<<"Tau:\t"<<aver<<"\t"<<sigma<<"\t"<<error<<" ps"<<endl;

    xvgPlot tauHisto(graphTau.getHistogram(0, timeStep, 0));
    tauHisto.addComment("Reference group: " + string(refsel_.name()));
    tauHisto.addComment("Selection group: " + string(sel_.name()));
    tauHisto.addComment("Cutoff: " + std::to_string(cutoff));
    tauHisto.addComment("Binwidth: " + std::to_string(timeStep));
    tauHisto.plot("histoTau.xvg");

    float area = 0, tau = 0;
    size_t nbins = tauHisto.npoints();
    vector<float> ty(nbins);
    for(size_t i = 0; i <  nbins; i++)
    {
    	ty[i] = tauHisto(0, i)*tauHisto(1, i);
    }
    tau = ty[0]/2.;
    area = tauHisto(1, 0)/2.;
    for(size_t i = 1; i < nbins-1; i++)
    {
    	tau += ty[i];
    	area += tauHisto(1, i);
    }
    tau+= ty[nbins-1]/2.;
    area+= tauHisto(1, nbins-1);
    tau*= timeStep;
    area*= timeStep;
    tau /= area;

    // gmx_stats_get_average( kinStats, &aver);
    // gmx_stats_get_sigma(   kinStats, &sigma);
    // gmx_stats_get_error(   kinStats, &error);
    // cout<<"Kin:\t"<<aver<<"\t"<<sigma<<"\t"<<error<<endl;

    // gmx_stats_get_average( koutStats, &aver);
    // gmx_stats_get_sigma(   koutStats, &sigma);
    // gmx_stats_get_error(   koutStats, &error);
    // cout<<"Kout:\t"<<aver<<"\t"<<sigma<<"\t"<<error<<endl;

    // gmx_stats_get_average(koutinStats, &aver);
    // gmx_stats_get_sigma(  koutinStats, &sigma);
    // gmx_stats_get_error(  koutinStats, &error);
    // cout<<"Kout-Kin:\t"<<aver<<"\t"<<sigma<<"\t"<<error<<endl;

    cout<<"\n\nTau = int tP(t)dt/int P(t)dt = "<<tau<<" ps"<<endl;

    gmx_stats_free(tauStats   );
    // gmx_stats_free(kinStats   );
    // gmx_stats_free(koutStats  );
    // gmx_stats_free(koutinStats);
}

void residenceTimeTool::writeOutput()
{

}


/*! \brief
 * The main function for the analysis template.
 */
int main(int argc, char *argv[])
{

	for(int i = 0; i < argc; ++i)
	{
		commandLineString+= argv[i];
		commandLineString+= " ";
	}

    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<residenceTimeTool>(argc, argv);
}
