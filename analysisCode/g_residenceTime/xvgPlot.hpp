#ifndef _XVG_PLOT_HPP_
#define _XVG_PLOT_HPP_

#include <vector>
#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <chrono>
#include <algorithm>

using namespace std;

#ifndef PRECISION
#define PRECISION 4 //default precision
#endif

static constexpr int dataPrecision = PRECISION;
static constexpr int dataLength = PRECISION + 5;

class xvgPlot
{
public:
	xvgPlot(const string& header, const string& cmdLine = ""): headerString(header), commandLine(cmdLine)
	{

	}
	xvgPlot(const xvgPlot& plot): headerString(plot.headerString), 
								  commandLine(plot.commandLine),
								  data(plot.data),
								  comments(plot.comments),
								  column_titles(plot.column_titles)
	{

	}

	xvgPlot(xvgPlot&& plot): headerString(std::move(plot.headerString)), 
							 commandLine(std::move(plot.commandLine)),
							 data(std::move(plot.data)),
							 comments(std::move(plot.comments)),
							 column_titles(std::move(plot.column_titles))
	{

	}

	xvgPlot& operator=(const xvgPlot& ) = default;
	xvgPlot& operator=(xvgPlot&& ) = default;

	void addColumn(const string& );

	template<class iterator>
	void addColumn(const string& str, const iterator& begin, const iterator& end)
	{
		column_titles.push_back(str);
		data.push_back(std::vector<double>(begin, end));
	}

	inline void addPoint(const size_t& i, const double& value) 
	{ 
		data[i].push_back(value);
	}
	
	inline void addComment(const string& comment)
	{
		comments.push_back(comment);
	}

	inline void setCommandLine(const string& cmdLine)
	{
		commandLine = commandLine;
	}

	inline const size_t npoints()
	{
		return data.at(0).size();
	}

	inline const size_t ncolumns()
	{
		return data.size();
	}

	inline const double& operator()(const size_t i, const size_t j)
	{
		return data[i][j];
	}

	xvgPlot getHistogram(const size_t& column, const float& dx, const size_t& bins);

	void plot(const string& );



private:
	string headerString, commandLine;

	vector< vector<double> > data;
	vector<string> comments, column_titles;
};

#endif // _XVG_PLOT_HPP_
