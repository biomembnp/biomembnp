/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

// OLD VERSION

#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <fstream>

#include <gromacs/trajectoryanalysis.h>
#include <gromacs/pbcutil/pbc.h>   //WARNING: IS THIS SUPPOSED TO BE INCLUDED?


using namespace gmx;
using namespace std;

string double_to_string(double a){
    ostringstream stream1;
    stream1<<a;
    return stream1.str();
}

string int_to_string(int a, int padding){
    ostringstream stream1;
    stream1<<setfill('0')<<setw(5)<<a;
    return stream1.str();
}

class cubic_box_adjust_to_pbc{    //it is implemented as a class to avoid passing box parameters at every function call
    public:
        cubic_box_adjust_to_pbc(vector<double>);
        vector <double> box;
        vector <double> adjust(vector<double>);
};

cubic_box_adjust_to_pbc::cubic_box_adjust_to_pbc(vector<double> box1){
    box=box1;
}


vector <double> cubic_box_adjust_to_pbc::adjust(vector <double> old_pos){
    vector<double> new_pos = old_pos;

    //adjusting positions for boundary conditions (TO DO: MUST CHECK!)
    for (int i = 0; i < 3; ++i)
    {
        while(new_pos[i]>=box[i]) new_pos[i]-=box[i];
        while(new_pos[i]<0) new_pos[i]+=box[i];
    }

    return new_pos;
}


class xvgPlot{

public:
void addColumn(string);
void addPoint(int, double);
void Plot(string);
void addComment(string);

private:
vector <string> column_titles;
vector <vector <double> > data;
vector <string> comments;
};

void xvgPlot::addColumn(string title){
	vector <double> column;
	data.push_back(column);
	column_titles.push_back(title);
}


void xvgPlot::addPoint(int column, double value){
	data[column].push_back(value);
}

void xvgPlot::addComment(string comment){
	comments.push_back("# "+comment);
}

void xvgPlot::Plot(string filename){

	FILE * output_file=fopen(filename.c_str(),"w");

	int n_columns= data.size();

	//verify all columns have the same number of elements:
	int n_rows=data[0].size();
	for (int i = 1; i < data.size(); ++i)  {
		if(data[i].size()!=n_rows) {
			fprintf(stderr,"xvgPlot: ERROR ! COLUMNS HAVE DIFFERENT NUMBER OF ELEMENTS.");
				exit(1);
		}
	}

	for(int i=0;i<comments.size();i++) fprintf(output_file,"%s\n",comments[i].c_str());

	for(int i=0;i<column_titles.size();i++) fprintf(output_file,"# column %i = %s\n",i,column_titles[i].c_str());


	for (int i = 0; i < n_rows; ++i)
	{
		for (int j = 0; j < n_columns; ++j)
		{
			fprintf(output_file,"%f \t\t",(float)data[j][i]);
		}
		fprintf(output_file,"\n");
	}

	fclose(output_file);

}




/*! \brief
 * Template class to serve as a basis for user analysis tools.
 */
class AnalysisTemplate : public TrajectoryAnalysisModule
{
    public:
        AnalysisTemplate();

        virtual void initOptions(IOptionsContainer *options,
                                 TrajectoryAnalysisSettings *settings);
        virtual void initAnalysis(const TrajectoryAnalysisSettings &settings,
                                  const TopologyInformation &top);

        virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                                  TrajectoryAnalysisModuleData *pdata);

        virtual void finishAnalysis(int nframes);
        virtual void writeOutput();

    private:
        class ModuleData;

        string                      filename_mask;
        Selection                        refsel_;
        Selection                           sel_;

		string plot_type;
		string axis_string;
		int axis;
		int coord1, coord2;
        int n_frames_average;
        int n_segments;
        int n_segments_side;
        float radius_max;
        float segment_length;
        vector <vector <double> > segment_kin_energies;
        float R;
        float axis_min, axis_max;
        string log_file;
        int n_plots;   //number of drawn plots;
};


AnalysisTemplate::AnalysisTemplate()
{
	R=8.3144598 ; //gas constant, J/(K mol)
    fprintf(stderr,"ANALYSIS_TEMPLATE()\n");
}


void AnalysisTemplate::initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings)
{
    static const char *const desc[] = 
    {
        "This is a template"
    };

    fprintf(stderr,"SETTING OPTIONS\n");

    settings->setFrameFlags((1<<0) | (1<<1)| (1<<2) | (1<<3) );   // strano che occorra scrivere il valore esplicito di questi flag... non c'e' un altro modo?

    settings->setHelpText(desc);

    options->addOption(FileNameOption("o")
    						.defaultBasename("temperature")
    						.store(&filename_mask).defaultBasename("temperature_plot_").required()
    						.description("Plot of average temperature in different sections of the system (Multiple numbered files will be generated)"));  // File backup has not been implemented yet.
                           	//.outputFile()

    options->addOption(FileNameOption("log")
   .filetype(eftPlot).outputFile()
   .store(&log_file).defaultBasename("temperature_plot_log").required()
   .description("Temperature analysis log file"));

    const char * const  allowed_plot_types[] = { "spherical", "cylindrical", "slabs","single_slab_2D" };
    options->addOption(StringOption("plot_type")
    						.enumValue(allowed_plot_types)
							.store(&plot_type)
    						.required()
    						.description("Plotting mode for temperature averaging"));

    options->addOption(IntegerOption("n_segments")
    						.store(&n_segments)
    						.required()
    						.description("Number of segments for temperature averaging (if single_slab_2D mode has been selected insert number of segments per side)"));

    options->addOption(IntegerOption("n_frames_average")
    						.store(&n_frames_average)
    						.required()
    						.description("Number of frames for each point of temperature averaging"));

    options->addOption(FloatOption("axis_min")
    					.store(&axis_min)
    					.required()
    					.description("lower plot boundary (with respect to the center of mass of the center group)"));

    options->addOption(FloatOption("axis_max")
    					.store(&axis_max)
    					.required()
    					.description("upper plot boundary (with respect to the center of mass of the center group)"));

    options->addOption(FloatOption("radius")
    					.store(&radius_max)
    					.required()
    					.description("Radius for spherical and cylindrical plotting modes (with respect to the center of mass of the center group)"));

    const char * const  allowed_axes[] = { "x", "y", "z" };
    options->addOption(StringOption("axis")
    						.enumValue(allowed_axes)
							.store(&axis_string)
    						.required()
    						.description("Axis for temperature averaging (except for spherical plot type)"));

    options->addOption(SelectionOption("reference")
                           .store(&refsel_)
                           .required()
                           .description("Reference group to center"));

    options->addOption(SelectionOption("select")
                           .store(&sel_)
                           .required()
                           .description("Group to consider for temperature averaging").evaluateVelocities());

    settings->setFlag(TrajectoryAnalysisSettings::efRequireTop);

}


void AnalysisTemplate::initAnalysis(const TrajectoryAnalysisSettings &settings,
                               const TopologyInformation         & /*top*/)
{
	fprintf(stderr, "INITIATING ANALYSIS\n");

	 	if(plot_type.compare("cylindrical")==0 || plot_type.compare("spherical")==0){
    		segment_length= (double)radius_max/(double)n_segments;
      } else if(plot_type.compare("slabs")==0) {
  			segment_length= (axis_max-axis_min)/(double)n_segments;
   	} else if(plot_type.compare("single_slab_2D")==0) {
			n_segments_side=n_segments;
			segment_length= radius_max*2/(double)n_segments;
		   n_segments=n_segments*n_segments;   // bisognerebbe gestire in modo diverso questa cosa.
		}

	//setting analysis parameters:
	//n_frames_average = 5;
	//n_segments = 10;
	//radius_max = 3; //nanometers
	//axis_min = - 5;
	//axis_max = + 5;

    if(axis_string.compare("x")==0) axis=0;
	else if(axis_string.compare("y")==0) axis=1;
	else if(axis_string.compare("z")==0) axis=2;
	coord1= (axis+1)%3;
	coord2= (axis+2)%3;

	vector <double> double_vec;
	for(int i=0;i<n_segments;++i) segment_kin_energies.push_back(double_vec);

    //initializing log file
    ofstream log_file_stream;
    log_file_stream.open(log_file.c_str());

    // printing analysis parameters on screen and log file
    cout<<"ANALYSIS PARAMETERS:"<<endl;
    cout<<"Temperature analysis data files: "<<filename_mask<<endl;
    cout<<"Temperature analysis log file: "<<log_file<<endl;
    cout<<"Reference group to center: "<<refsel_.name()<<endl;
    cout<<"Group to consider for temperature averaging: "<<sel_.name()<<endl;
    cout<<"Plotting mode for temperature averaging: "<<plot_type<<endl;
    cout<<"Number of segments for temperature averaging: "<<n_segments<<endl;
    cout<<"Number of frames for each point of temperature averaging : "<< n_frames_average <<endl;
    cout<<"Lower plot boundary (with respect to the center of mass of the center group): "<<axis_min<<endl;
	 cout<<"upper plot boundary (with respect to the center of mass of the center group): "<<axis_max<<endl;
	 cout<<"Radius for spherical and cylindrical plotting modes (with respect to the center of mass of the center group): "<<radius_max<<endl;
	 cout<<"Axis for temperature averaging (except for spherical plot type): "<<axis_string<<endl;
    cout<<endl<<endl;

    log_file_stream<<"ANALYSIS PARAMETERS:"<<endl;
    log_file_stream<<"Temperature analysis data files: "<<filename_mask<<endl;
    log_file_stream<<"Temperature analysis log file: "<<log_file<<endl;
    log_file_stream<<"Reference group to center: "<<refsel_.name()<<endl;
    log_file_stream<<"Group to consider for temperature averaging: "<<sel_.name()<<endl;
    log_file_stream<<"Plotting mode for temperature averaging: "<<plot_type<<endl;
    log_file_stream<<"Number of segments for temperature averaging: "<<n_segments<<endl;
    log_file_stream<<"Number of frames for each point of temperature averaging : "<< n_frames_average <<endl;
    log_file_stream<<"Lower plot boundary (with respect to the center of mass of the center group): "<<axis_min<<endl;
	 log_file_stream<<"upper plot boundary (with respect to the center of mass of the center group): "<<axis_max<<endl;
	 log_file_stream<<"Radius for spherical and cylindrical plotting modes (with respect to the center of mass of the center group): "<<radius_max<<endl;
	 log_file_stream<<"Axis for temperature averaging (except for spherical plot type): "<<axis_string<<endl;
    log_file_stream<<endl<<endl;

    log_file_stream.close();

	 n_plots=0;
}

void AnalysisTemplate::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata)
{
  //if(frnr>13000) fprintf(stderr, "\nANALYZING FRAME %i\n",frnr);
    const Selection           &refsel = pdata->parallelSelection(refsel_);
    const Selection           &sel    = pdata->parallelSelection(sel_);

	//ensuring that periodic boundary conditions are enforced

		  //extracting (cubic) box dimensions
	    vector <double> box(3);
	    for (int i = 0; i < 3; ++i) box[i]=pbc->box[i][i];

		 //initializing pbc adjuster
       cubic_box_adjust_to_pbc pbc_adjuster(box);

		 //adjusting refsel positions
		 int refsel_nr=refsel_.posCount();
		 vector <vector <double> >  adjusted_refsel_p;
		 for (int i = 0; i < refsel_nr; ++i) {
		 	 SelectionPosition p = refsel.position(i);
	       vector <double> r;
	       for (int j = 0; j < 3; ++j) r.push_back(p.x()[j]);
	       adjusted_refsel_p.push_back(pbc_adjuster.adjust(r));
		 }


   	 //adjusting sel positions
   	 int sel_nr=sel_.posCount();
		 vector <vector <double> >  adjusted_sel_p;
		 for (int i = 0; i < sel_nr; ++i) {
		 	 SelectionPosition p = sel.position(i);
		    vector <double> r;
	       for (int j = 0; j < 3; ++j) r.push_back(p.x()[j]);

	       adjusted_sel_p.push_back(pbc_adjuster.adjust(r));
		 }

    //finding geometric center of center group
    double x_c[3];
    //int refsel_nr = refsel.posCount();
    for (int g = 0; g < refsel_nr; ++g)  {
    	// SelectionPosition p = refsel.position(g); questa istruzione non dovrebbe servire a nulla; perche' era qui? forse perche' volevo metterci la massa ma  poi mi sono dimenticato
    	for(int i=0;i<3;++i) x_c[i]+=adjusted_refsel_p[g][i];
    }
    for(int i=0;i<3;++i) x_c[i] /= refsel_nr;


   //if(frnr > 13000) fprintf(stderr,"Center of mass found\n");

    //Calculating radially-averaged temperature distribution
//    if (fr.bV)  fprintf(stderr,"Frame has velocities.\n"); else fprintf(stderr,"Frame has no velocities!\n");
//    if (sel.hasVelocities())  fprintf(stderr,"Velocities found.\n"); else fprintf(stderr,"Velocities missing!\n");
    //int sel_nr   = sel.posCount();

    	//now the temperature averaging is made according to plot_type selected. The code may be redundant for clarity purposes.
    	if(plot_type.compare("cylindrical")==0){

	    	for (int i = 0; i < sel_nr; ++i)  {
	    		SelectionPosition p = sel.position(i);
	    		if(adjusted_sel_p[i][axis]-x_c[axis]> axis_min && adjusted_sel_p[i][axis]-x_c[axis]< axis_max ){
	    			double squared_distance=0;
	    			for(int j=0; j<3 ; ++j) if(j!=axis) squared_distance+= (adjusted_sel_p[i][j]-x_c[j])*(adjusted_sel_p[i][j]-x_c[j]); //the distance is projected on a plane according to the selected axis.
	    			double distance = sqrt(squared_distance);
	    			if(distance<radius_max){
	    				int segment= (int) floor( distance / segment_length);
	    				double kin_energy = 0.5* (p.v()[0]*p.v()[0]+p.v()[1]*p.v()[1]+p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
	    				segment_kin_energies[segment].push_back(kin_energy);
	    			}
	    		}
	    	}

		} else if(plot_type.compare("spherical")==0){
	       	for (int i = 0; i < sel_nr; ++i)  {
	    		SelectionPosition p = sel.position(i);   // non serve per la posizione (perche' prendo quella adjusted, ma solo per velocita' e massa)
	    		double squared_distance=0;
	    		for(int j=0; j<3 ; ++j) squared_distance+= (adjusted_sel_p[i][j]-x_c[j])*(adjusted_sel_p[i][j]-x_c[j]);
	    		double distance = sqrt(squared_distance);
	    		if(distance<radius_max){
	    			int segment= (int) floor( distance / segment_length);
	    			double kin_energy = 0.5* (p.v()[0]*p.v()[0]+p.v()[1]*p.v()[1]+p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
	    			segment_kin_energies[segment].push_back(kin_energy);
	    		}
	    	}
	    } else if(plot_type.compare("slabs")==0) {

	    	for (int i = 0; i < sel_nr; ++i)  {
	    		SelectionPosition p = sel.position(i);
	    		if(adjusted_sel_p[i][axis]-x_c[axis]> axis_min + fabs(axis_min*0.00001) && adjusted_sel_p[i][axis]-x_c[axis]< axis_max - fabs(axis_max*0.00001)){
    				int segment= (int) floor( (adjusted_sel_p[i][axis]-x_c[axis]-axis_min) / segment_length);
    				double kin_energy = 0.5* (p.v()[0]*p.v()[0]+p.v()[1]*p.v()[1]+p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
    				segment_kin_energies[segment].push_back(kin_energy);
	    		}
	    	}
	    }  else if(plot_type.compare("single_slab_2D")==0) {

	    	for (int i = 0; i < sel_nr; ++i)  {
	    		SelectionPosition p = sel.position(i);
	    		if(adjusted_sel_p[i][axis]-x_c[axis]> axis_min && adjusted_sel_p[i][axis]-x_c[axis]< axis_max && fabs(adjusted_sel_p[i][coord1]-x_c[coord1]) < radius_max*0.99999  && fabs(adjusted_sel_p[i][coord2]-x_c[coord2]) < radius_max *0.99999 ){ // 0.99999 is to avoid that (for some unexplained reason) particles out of the drawing zone are considered too.
    			//	if(frnr>=13635) fprintf(stdout,"frnr=%i CHECKPOINT1\n segment_kin_energies.size()=%d\n",frnr,(int) segment_kin_energies.size());
    				int segment= (int) floor( (adjusted_sel_p[i][coord1]-x_c[coord1]+radius_max) / segment_length)*n_segments_side + (int) floor( (adjusted_sel_p[i][coord2]-x_c[coord2]+radius_max) / segment_length);
    			//	if(frnr>=13635) fprintf(stdout,"%d %f %f \n",n_segments_side, floor( (adjusted_sel_p[i][coord1]-x_c[coord1]+radius_max) / segment_length),floor( (adjusted_sel_p[i][coord2]-x_c[coord2]+radius_max) / segment_length));
    			//	if(frnr>=13635) fprintf(stdout,"segment_kin_energies[%d].size()=%d\n",segment,(int)segment_kin_energies[segment].size());
    				double kin_energy = 0.5* (p.v()[0]*p.v()[0]+p.v()[1]*p.v()[1]+p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
    				//fprintf(stderr, "segment %d, n_segments_side %d\n",segment,n_segments_side);
    			//	if(frnr>=13635) fprintf(stdout,"CHECKPOINT2\n");
    				segment_kin_energies[segment].push_back(kin_energy);
    			//	if(frnr>=13635) fprintf(stdout,"CHECKPOINT3\n\n");
    			}
	    	}
	    }

	//if(frnr>13000) fprintf(stderr,"kinetic energies added\n");

 	if(frnr%n_frames_average==0  &&  frnr>= n_frames_average-1 ){      //graph plotting

 	xvgPlot graph;

 	if(plot_type.compare("cylindrical")==0 || plot_type.compare("spherical")==0){
    	graph.addColumn((string)"Radius");
    		for (int i = 0; i < n_segments; ++i) graph.addPoint(0,segment_length*(i+0.5));
		graph.addColumn("Temperature");
   	} else if(plot_type.compare("slabs")==0) {
  		graph.addColumn((string)"z");
    		for (int i = 0; i < n_segments; ++i) graph.addPoint(0,axis_min+segment_length*(i+0.5));
    	graph.addColumn("Temperature");
    	} else if(plot_type.compare("single_slab_2D")==0) {
    		for (int i=0; i < n_segments_side; ++i)	graph.addColumn(double_to_string(segment_length*(i+0.5)-radius_max)+" nm");
		}

	graph.addComment("Plot_type = " + plot_type);
	graph.addComment("Frame_number = " + int_to_string(frnr, 1) );
	graph.addComment("Time = " + int_to_string(fr.time,1)+" ps" );

 		for (int i = 0; i < n_segments; ++i)	{
 			double sum_kin_energy = 0 ;
 			int n_kin_energies = segment_kin_energies[i].size();
 			double temperature = 0;
 			if(n_kin_energies!=0){
 			for (int j = 0; j < n_kin_energies; ++j) sum_kin_energy += segment_kin_energies[i][j];
 			double average_kin_energy = sum_kin_energy/ (double) n_kin_energies;  // average kinetic energy per particle.
 			temperature = average_kin_energy * (2.0/3.0) / R * 1000 ;
 			}

 			if(plot_type.compare("single_slab_2D")==0){   //attenzione, l'immagine potrebbe essere girata.
 				int segment_y = (int) floor((float)i/n_segments_side);
 				graph.addPoint(segment_y, temperature);
 			} else {
 			graph.addPoint(1, temperature);
 			}


 			segment_kin_energies[i].clear();
 		}

		string plot_filename = filename_mask + int_to_string(n_plots,6) + ".xvg";   //padding should be made automatic
		graph.Plot(plot_filename);

		n_plots++;
 	}
}

void AnalysisTemplate::finishAnalysis(int /*nframes*/)
{

}

void AnalysisTemplate::writeOutput()
{

}

/*! \brief
 * The main function for the analysis template.
 */
int main(int argc, char *argv[])
{
    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<AnalysisTemplate>(argc, argv);
}
