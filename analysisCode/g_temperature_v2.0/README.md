# README #

GROMACS tool to calculate temperature maps!

Created by Andrea Torchi and Sebastian Salassi

## Compile and install ##
It depends on gromacs v. 2016.x and C++11
Compile with
`make`


## Issues and bugs ##

Please use the issues tracker to post bugs and whatever!
