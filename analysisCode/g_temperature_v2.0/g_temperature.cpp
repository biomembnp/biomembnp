/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <numeric>
#include <algorithm>

#include <gromacs/version.h>
#include <gromacs/trajectoryanalysis.h>
#include <gromacs/pbcutil/pbc.h>
#include <gromacs/fileio/trxio.h>
#include <gromacs/topology/mtop_util.h>

#include "xvgPlot.hpp"

using namespace gmx;
using namespace std;

//gas constant, J/(K mol)
#define kRGas 8.3144598 

//From kJ/mol to K
static constexpr double kKinTemp = 2.0 / kRGas * 1000.;

static const char *const description[] = 
    {
        "Temperature analysis tool. v. 2.0\n",
        "Written by Andrea Torchi. Maintained by Sebastian Salassi\n",
        "Please cite:\n",
        "Local Enhancement of Lipid Membrane Permeability Induced by Irradiated Gold Nanoparticles, ",
        "Torchi A. et al., ACS Nano 2017, 11, 12553-12561",
        "DOI: 10.1021/acsnano.7b06690\n\n",
        "Water dynamics affects thermal transport at the surface of hydrophobic and hydrophilic irradiated nanoparticles,",
        "S. Salassi et. al., Nanoscale Adv., 2020 ",
        "DOI: https://doi.org/10.1039/D0NA00094A" 
    };

static string commandLineString = "";

static const string plotHeaderString =
	"# ***************************************\n"\
	"# *      Temperature analysis tool      *\n"\
    "# * Andrea Torchi and Sebastian Salassi *\n"\
    "# ***************************************\n\n"\
    "# Please cite:\n"\
    "# 1) Local Enhancement of Lipid Membrane Permeability Induced by Irradiated Gold Nanoparticles,\n"\
    "#    Torchi A. et al., ACS Nano 2017, 11, 12553-12561\n"\
    "#    DOI: 10.1021/acsnano.7b06690\n"\
    "# 2) Water dynamics affects thermal transport at the surface of hydrophobic and hydrophilic irradiated nanoparticles,\n"\
    "#    S. Salassi, A. Cardellini, P. Asinari, R. Ferrando and G. Rossi\n"\
    "#    Nanoscale Adv., 2020\n"\
    "#    DOI: https://doi.org/10.1039/D0NA00094A";


bool checkResName(t_atoms *at, const size_t index, const string resN);
bool checkAtomName(t_atoms *at, const size_t index, const string aName);
void makeTemperature(vector< vector<double> >& segmentKins, vector< pair<double, double> >& temperature, vector<double> dof);

class temperatureAnalysisTool : public TrajectoryAnalysisModule
{
public:
    temperatureAnalysisTool();

#if (GMX_API_VERSION < 20160000)
    virtual void initOptions(Options *options, TrajectoryAnalysisSettings *settings);
#else
    virtual void initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings);
#endif
    virtual void optionsFinished(TrajectoryAnalysisSettings *settings);
    virtual void initAfterFirstFrame(const TrajectoryAnalysisSettings &settings, const t_trxframe &fr);
    virtual void initAnalysis(const TrajectoryAnalysisSettings &settings, const TopologyInformation &top);

    virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata);

    virtual void finishAnalysis(int nframes);
    virtual void writeOutput();

private:
    string outputFilename, outputTimeFilename;
    
    Selection refsel_;
    Selection sel_;

    string plot_type, axis_string;
    size_t axis, coord1, coord2;
    size_t n_segments, n_segments_side;
    float binwidth, axis_min, axis_max;
    float stride, timeStep;
    //float nDOFSol;
    bool flexibleWater;
    bool flexibleHydrogen;
    
    vector< vector<double> > segment_kin_energies;
    vector< double > segment_DOF;
    vector< vector< pair<double, double> > > outputTemperature;

    vector<float> timeDistances;
    vector< pair<double, vector<float> > > timeTemperatures;

    t_atoms atoms;
};

temperatureAnalysisTool::temperatureAnalysisTool() 
    : axis_string("z"), axis(2), coord1(0), coord2(1)
    , n_segments(0), n_segments_side(0), binwidth(0.1)
    , axis_min(0.), axis_max(0.), stride(0.), timeStep(0.)
    , flexibleWater(false), flexibleHydrogen(false)
    //, nDOFSol(9.)
#if (GMX_API_VERSION < 20160000)
    , TrajectoryAnalysisModule("g_temperature", "Temperature analysis tool")
#endif
{

}

#if (GMX_API_VERSION < 20160000)
void temperatureAnalysisTool::initOptions(Options *options, TrajectoryAnalysisSettings *settings)
#else
void temperatureAnalysisTool::initOptions(IOptionsContainer *options, TrajectoryAnalysisSettings *settings)
#endif
{
#if (GMX_API_VERSION < 20160000)
    options->setDescription(description);
#else
    settings->setHelpText(description);
#endif

    const char* const  allowed_plot_types[] = {"spherical"};//, "cylindrical", "slabs", "single_slab_2D" };
    const char* const  allowed_axes[] = { "x", "y", "z" };

    settings->setFlags(TRX_READ_X | TRX_NEED_X | TRX_READ_V | TRX_NEED_V);
    settings->setFrameFlags(TRX_READ_X | TRX_NEED_X | TRX_READ_V | TRX_NEED_V);

    settings->setPBC(true);
    settings->setRmPBC(false);
    settings->setFlag(TrajectoryAnalysisSettings::efRequireTop | 
                      TrajectoryAnalysisSettings::efNoUserPBC  |
                      TrajectoryAnalysisSettings::efNoUserRmPBC );

    options->addOption(FileNameOption("o").outputFile().filetype(eftGenericData)
             .store(&outputFilename).defaultBasename("temperature")
             .description("Plot of average temperature"));
    options->addOption(FileNameOption("ot").outputFile().filetype(eftPlot)
             .store(&outputTimeFilename).defaultBasename("temperatureTime")
             .description("Plot the temperature in function of the time for a specified shell."));
    options->addOption(FloatOption("distances").storeVector(&timeDistances).multiValue()
             .description("Distances (respect to reference COM) for which calculate temperature as a function of the time (nm)") );
    options->addOption(StringOption("plot").enumValue(allowed_plot_types).store(&plot_type).defaultValue("spherical") //.required()
             .description("Plotting mode for temperature averaging"));
    options->addOption(FloatOption("bin").store(&binwidth).defaultValue(0.1)
             .description("Bin width (nm)").required());
    options->addOption(FloatOption("min").store(&axis_min).defaultValue(0.).required()
             .description("Lower plot boundary, respect to reference COM (0 means the maximum allowed) (nm)"));
    options->addOption(FloatOption("max").store(&axis_max)
             .description("Upper plot boundary, respect to reference COM (0 means the maximum allowed) (nm)"));
    //options->addOption(StringOption("axis").enumValue(allowed_axes).store(&axis_string)
    //         .defaultValue("z").description("Axis for temperature averaging (except for spherical plot type)"));
    options->addOption(FloatOption("stride").store(&stride).defaultValue(0.)
             .description("Time interval in which average temperature (must be greater than trajectory time step) (ps)"));
    options->addOption(SelectionOption("reference").store(&refsel_).onlyAtoms()
             .description("Reference group to center").required());
    options->addOption(SelectionOption("select").required().onlyAtoms()
    	     .store(&sel_).required().evaluateVelocities()
             .description("Group to consider for temperature averaging"));
    //options->addOption(FloatOption("nc").store(&nDOFSol).defaultValue(3)
    //		 .description("Number of degree of freedom per atoms per solvent molecules. \
    		 	Rigid water model have 2 DOF per atom per molecule. Full-flexible model have 3 DOF per atom per molecule. Semi-flexible model have 2.333 DOF per atom per molecule."));
    options->addOption(BooleanOption("flexible-water").store(&flexibleWater).defaultValue(false)
             .defaultValueIfSet(true).description("yes if flexible water model is used"));
    options->addOption(BooleanOption("flexible-h").store(&flexibleHydrogen).defaultValue(false)
             .defaultValueIfSet(true).description("yes if all hydrogen bonds are not constraint"));
}

void temperatureAnalysisTool::optionsFinished(TrajectoryAnalysisSettings *settings)
{
    if(outputFilename.empty() && outputTimeFilename.empty())
    {
        GMX_THROW(InconsistentInputError("An output option (-o and/or -ot) must be specified."));
    }
    if(plot_type == "single_slab_2D")
    {
        GMX_THROW(InconsistentInputError("The single_slab_2D plot type is not yet implemented."));
    }
    if(!outputTimeFilename.empty() && timeDistances.empty())
    {
        GMX_THROW(InconsistentInputError("The option -ot need at least one value in -distances."));
    }
}

void temperatureAnalysisTool::initAnalysis(const TrajectoryAnalysisSettings &settings, const TopologyInformation &top)
{
    if(axis_string == "x") 
        axis = 0;
    else if(axis_string == "y") 
        axis = 1;
    else if(axis_string == "z") 
        axis = 2;

    coord1 = (axis+1)%3;
    coord2 = (axis+2)%3;

    if(!outputTimeFilename.empty())
    {
        for(auto& it : timeDistances)
        {
            it = (int)(it/binwidth-1);
        }
    }

	//t_idef idef;
    if(top.hasTopology())
    {
    	atoms = gmx_mtop_global_atoms(top.mtop());
    	//idef = top.topology()->idef;
    } else
    {
    	GMX_THROW(InconsistentInputError("The option -s must be specified."));
    }
}

void temperatureAnalysisTool::initAfterFirstFrame(const TrajectoryAnalysisSettings &settings, const t_trxframe &fr)
{
    //Initialize maximum distance if not set
    if(axis_max <= 0.)
    {
        if(settings.hasPBC())
            axis_max = sqrt(0.998*0.998*max_cutoff2(epbcXYZ, fr.box));
        else
        {
            if(plot_type == "spherical")
                axis_max = std::min(fr.box[XX][XX], std::min(fr.box[YY][YY], fr.box[ZZ][ZZ]))/2.;
            else //if(plot_type == "cylindrical" || plot_type == "slabs")
            {
                axis_max = std::min(fr.box[coord1][coord1], fr.box[coord2][coord2])/2.;
            }
        }
    }

    //TODO
    if(plot_type == "single_slab_2D") 
    {
        n_segments = std::min(fr.box[coord1][coord1], fr.box[coord2][coord2])/binwidth;
        n_segments_side = n_segments;
        n_segments = n_segments*n_segments;
    }

    //Initialize the kin_energies container
    const size_t n = ceil((axis_max - axis_min) / binwidth);
    segment_kin_energies.assign(n, vector<double>());
    segment_DOF.assign(n, 0.);

    //Get first time
    timeStep = fr.time;
}

void temperatureAnalysisTool::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, TrajectoryAnalysisModuleData *pdata)
{
    // Convert stride from ps to number of frame
    if(frnr == 1 && stride > 0)
    {   
        timeStep = fr.time - timeStep;
        if(stride <= timeStep)
        {
            GMX_THROW(InconsistentInputError("The argument of the the option -stride must be grater then the trajectory time step."));
        }
        stride = (int)(stride / timeStep);
    }
    const Selection &refsel = pdata->parallelSelection(refsel_);
    const Selection &sel    = pdata->parallelSelection(sel_);

    //*** Ensuring that periodic boundary conditions are enforced
    //*** This works also for triclinic boxes
    const size_t refsel_nr = refsel_.posCount();

#if (GMX_API_VERSION >= 20180000)
    vector< BasicVector<float> > adjusted_refsel_p(refsel_nr);
#else
    vector< rvec > adjusted_refsel_p(refsel_nr);
#endif

    for(size_t i = 0; i < refsel_nr; ++i)
    {   
        adjusted_refsel_p[i][0] = refsel.coordinates()[i][0];
        adjusted_refsel_p[i][1] = refsel.coordinates()[i][1];
        adjusted_refsel_p[i][2] = refsel.coordinates()[i][2];
    }

#if (GMX_API_VERSION >= 20180000)    
    ArrayRef< BasicVector<float> > gmx_adjusted_refsel_p(adjusted_refsel_p);
    put_atoms_in_box(pbc->ePBC, pbc->box, gmx_adjusted_refsel_p);
#else
    put_atoms_in_box(pbc->ePBC, pbc->box, refsel_nr, adjusted_refsel_p.data());
#endif

    // adjusting sel positions
    const size_t sel_nr = sel_.posCount();

#if (GMX_API_VERSION >= 20180000)
    vector< BasicVector<float> > adjusted_sel_p(sel_nr);
#else
    vector< rvec > adjusted_sel_p(sel_nr);
#endif

    for(size_t i = 0; i < sel_nr; ++i)
    {   
        adjusted_sel_p[i][0] = sel.coordinates()[i][0];
        adjusted_sel_p[i][1] = sel.coordinates()[i][1];
        adjusted_sel_p[i][2] = sel.coordinates()[i][2];
    }

#if (GMX_API_VERSION >= 20180000)    
    ArrayRef< BasicVector<float> > gmx_adjusted_sel_p(adjusted_sel_p);
    put_atoms_in_box(pbc->ePBC, pbc->box, gmx_adjusted_sel_p);
#else
    put_atoms_in_box(pbc->ePBC, pbc->box, sel_nr, adjusted_sel_p.data());
#endif

    //*** Calc geometric center of reference selection
    rvec x_c = {0, 0, 0};
    for(auto it : adjusted_refsel_p)  
    {
        x_c[0] += it[0];
        x_c[1] += it[1];
        x_c[2] += it[2];
    }
    x_c[0] /= refsel_nr;
    x_c[1] /= refsel_nr;
    x_c[2] /= refsel_nr;

    map<size_t, double > segment_kin_energy_perFrame;
    map<size_t, double > segment_DOF_perFrame;

    //Calculating radially-averaged temperature distribution
    //now the temperature averaging is made according to plot_type selected. The code may be redundant for clarity purposes.
    // if(plot_type == "cylindrical")
    // {
    //     for(size_t i = 0; i < sel_nr; ++i)
    //     {
    //         //Get distance vector based on the pbc (works also for triclinic boxes)
    //         rvec dx;
    //         pbc_dx(pbc, adjusted_sel_p[i], x_c, dx);
    //         if(dx[axis] > axis_min && dx[axis] < axis_max )
    //         {
    //             const double distance = sqrt(dx[coord1]*dx[coord1] + dx[coord2]*dx[coord2]);
    //             if(distance >= axis_min && distance < axis_max)
    //             {
    //                 const SelectionPosition p = sel.position(i);
    //                 //any virtual sites has not be counted, they have mass equal to 0!
    //                 if(p.mass() != 0)
    //                 {
    //                     const size_t segment = distance/binwidth;
    //                     const double kin_energy = 0.5 * ( p.v()[0]*p.v()[0] + p.v()[1]*p.v()[1] + p.v()[2]*p.v()[2] )*p.mass();  // (KJ/mol)
    //                     segment_kin_energies[segment].push_back(kin_energy);
    //                     segment_kin_energy_perFrame[segment] += kin_energy;
    //                 }
    //             }

    //             if(!outputTimeFilename.empty())
    //             {
    //                 vector<float> tempMean(timeDistances.size(), 0);
    //                 for(size_t i = 0; i < timeDistances.size(); ++i)
    //                 {
    //                     double sum = segment_kin_energy_perFrame[timeDistances[i]];
    //                     //auto values = segment_kin_energy_perFrame[timeDistances[i]];
    //                     //sum = std::accumulate(values.begin(), values.end(), sum);
    //                     tempMean[i] = sum*kKinTemp/segment_DOF_perFrame[timeDistances[i]];
    //                 }
        
    //                 timeTemperatures.push_back(make_pair(fr.time,  tempMean));
    //                 segment_kin_energy_perFrame.clear();
    //             }
    //         }
    //     }
    // } else if(plot_type == "spherical")
    // {
        for (size_t i = 0; i < sel_nr; ++i)
        {
            //Get distance vector based on the pbc (works also for triclinic boxes)
            rvec dx;
            pbc_dx(pbc, adjusted_sel_p[i], x_c, dx);
            const double distance = sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]);

            if(distance >= axis_min && distance < axis_max)
            {
                const SelectionPosition p = sel.position(i);

                //any virtual sites has not be counted, they have mass equal to 0!
                if(p.mass() != 0) 
                {
                    const size_t segment = distance/binwidth;
                    float segDOF = 3;
                    double kin_energy = 0;
                    
                    kin_energy = 0.5*(p.v()[0]*p.v()[0] + p.v()[1]*p.v()[1] + p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
                    
                    if( !flexibleWater && (checkAtomName(&atoms, p.atomIndices()[0], "HW1") || checkAtomName(&atoms, p.atomIndices()[0], "HW2")) )
                    {
                        segDOF = 1.5;
                    }
                    else if( !flexibleHydrogen && (checkAtomName(&atoms, p.atomIndices()[0], "HO") || checkAtomName(&atoms, p.atomIndices()[0], "OH")) )
    				{
    				  	segDOF = 2.5;
    				}
                    
                    segment_kin_energies[segment].push_back(kin_energy);
                    segment_kin_energy_perFrame[segment] += kin_energy;
    				segment_DOF[segment] += segDOF;
    				segment_DOF_perFrame[segment] += segDOF;
                }
            }
        }

        if(!outputTimeFilename.empty())
        {
            vector<float> tempMean(timeDistances.size(), 0);
            for(size_t i = 0; i < timeDistances.size(); ++i)
            {
                double sum = segment_kin_energy_perFrame[timeDistances[i]];
                //auto values = segment_kin_energy_perFrame[timeDistances[i]];
                //sum = std::accumulate(values.begin(), values.end(), sum);
                tempMean[i] = sum*kKinTemp/segment_DOF_perFrame[timeDistances[i]];
            }

            timeTemperatures.push_back(make_pair(fr.time,  tempMean));
            segment_kin_energy_perFrame.clear();
            segment_DOF_perFrame.clear();
        }

    // } else if(plot_type == "slabs")
    // {
    //     for (size_t i = 0; i < sel_nr; ++i)
    //     {
    //         //Get distance vector based on the pbc (works also for triclinic boxes)
    //         rvec dx;
    //         pbc_dx(pbc, adjusted_sel_p[i], x_c, dx);
    //         if(dx[axis] >= axis_min + fabs(axis_min*0.00001) && dx[axis] < axis_max - fabs(axis_max*0.00001))
    //         {
    //             const SelectionPosition p = sel.position(i);
    //             //any virtual sites has not be counted, they have mass equal to 0!
    //             if(p.mass() != 0)
    //             {
    //                 const size_t segment = static_cast<size_t>( (adjusted_sel_p[i][axis]-x_c[axis]-axis_min) / binwidth);
    //                 const double kin_energy = 0.5* (p.v()[0]*p.v()[0] + p.v()[1]*p.v()[1] + p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
    //                 segment_kin_energies[segment].push_back(kin_energy);
    //             }
    //         }
    //     }
    // } else if(plot_type == "single_slab_2D")
    // {
    //     /*
    //     for (size_t i = 0; i < sel_nr; ++i)
    //     {
    //         const SelectionPosition p = sel.position(i);
    //         if(adjusted_sel_p[i][axis]-x_c[axis] > axis_min && adjusted_sel_p[i][axis]-x_c[axis] < axis_max && fabs(adjusted_sel_p[i][coord1]-x_c[coord1]) < radius*0.99999  && fabs(adjusted_sel_p[i][coord2]-x_c[coord2]) < radius *0.99999 )
    //         {// 0.99999 is to avoid that (for some unexplained reason) particles out of the drawing zone are considered too.
    //             size_t segment = (size_t) floor( (adjusted_sel_p[i][coord1]-x_c[coord1]+radius) / binwidth)*n_segments_side + (int) floor( (adjusted_sel_p[i][coord2]-x_c[coord2]+radius) / binwidth);
    //             double kin_energy = 0.5* (p.v()[0]*p.v()[0] + p.v()[1]*p.v()[1] + p.v()[2]*p.v()[2])*p.mass();  // (KJ/mol)
    //             segment_kin_energies[segment].push_back(kin_energy);
    //         }
    //     }
    //     */
    // }

    //################################################
    //#Analyze every stride frame
    //################################################
    //if(stride > 0)
    //{
    // This maybe depend on compiler. gcc works fine
    if(stride > 0 && (frnr % static_cast<int>(stride)) == 0 && frnr >= (stride - 1) && frnr > 0 )
    {
        vector< pair<double, double> > temperature;
        makeTemperature(segment_kin_energies, temperature, segment_DOF);
        outputTemperature.push_back(temperature);
        
        //clear kin energies vector
        for(auto& it : segment_kin_energies)
        {
            it.clear();
        }
        //clean dof vector
        std::fill(segment_DOF.begin(), segment_DOF.end(), 0.);
    }
	//}
}

void temperatureAnalysisTool::finishAnalysis(int nframes)
{
    if(!outputTimeFilename.empty())
    {
        xvgPlot graphTime(plotHeaderString, commandLineString);

        graphTime.addComment("Plot type: " + plot_type);
        graphTime.addComment("Bin width: " + std::to_string(binwidth) + " nm");

        graphTime.addColumn("Time [ps]");
        for(auto it : timeDistances)
        {
            graphTime.addColumn("Temperature @ " + std::to_string((it+1)*binwidth) + " nm [K]");
        }

        for(auto it : timeTemperatures)
        {
            graphTime.addPoint(0, it.first); //time

            for(size_t i = 0; i < it.second.size(); ++i)
            {
                graphTime.addPoint(i+1, it.second[i]); //temp value
            }
        }
            
        graphTime.plot(outputTimeFilename);
    }

    if(!outputFilename.empty())
    {
        if(stride == 0)
        {
            vector< pair<double, double> > temperature;
            makeTemperature(segment_kin_energies, temperature, segment_DOF);
            outputTemperature.push_back(temperature);
        }

        //The file name is: base_name.dat
        string baseName = outputFilename.substr(0, outputFilename.size()-4);
        for(size_t i = 0; i < outputTemperature.size(); ++i)
        {
            //graph plotting
            xvgPlot graph(plotHeaderString, commandLineString);

            graph.addComment("Plot type: " + plot_type);
            graph.addComment("Averaged frames: " + std::to_string(static_cast<int>(stride == 0 ? nframes : stride)) );
            graph.addComment("Bin width: "  + std::to_string(binwidth) + " nm");
            graph.addComment("Min radius: " + std::to_string(axis_min) + " nm");
            graph.addComment("Max radius: " + std::to_string(axis_max) + " nm");
        
            const size_t segmentSize = segment_kin_energies.size();
            //*** Inserting x-axis
            if(plot_type == "cylindrical" || plot_type == "spherical")
            {
                graph.addColumn("Radius [nm]");
                for(size_t i = 1; i <= segmentSize; ++i)
                {
                    graph.addPoint(0, binwidth*i);
                }
                graph.addColumn("Temperature [K]");
                graph.addColumn("StdError [K]");
            } else if(plot_type == "slabs")
            {
                graph.addColumn("z");
                for(size_t i = 1; i <= segmentSize; ++i)
                {
                    graph.addPoint(0, axis_min+binwidth*i);
                }
                graph.addColumn("Temperature [K]");
                graph.addColumn("StdError [K]");
            } else if(plot_type == "single_slab_2D")
            {
                for(size_t i = 1; i <= segmentSize; ++i)
                {
                    graph.addColumn(std::to_string(binwidth*i-axis_max)+" nm");
                }
            }
    
            for(size_t j = 0; j < outputTemperature[i].size(); ++j)
            {
                //*** Inserting y-axis (temperature)
                if(plot_type == "single_slab_2D")
                {   
                    //attenzione, l'immagine potrebbe essere girata. ????
                    const int segment_y = floor((float)j/n_segments_side);
                    graph.addPoint(segment_y, outputTemperature[i][j].first);
                } else 
                {
                    graph.addPoint(1, outputTemperature[i][j].first);
                    graph.addPoint(2, outputTemperature[i][j].second);
                }
            }
        
            if(stride == 0)
                graph.plot(outputFilename);
            else
                graph.plot(baseName + "_" + std::to_string(static_cast<int>((i+1)*stride*timeStep)) + ".dat");
        }
    }
}

void temperatureAnalysisTool::writeOutput()
{

}


void makeTemperature(vector< vector<double> >& segmentKins, vector< pair<double, double> >& temperature, vector<double> dof)
{
    const size_t segmentSize = segmentKins.size();

    temperature.assign(segmentSize, make_pair(0.,0.));

    //*** Calculating and inserting y-axis (temperature)
    for(size_t i = 0; i < segmentSize; ++i)
    {
        //pair<double,double> temperature(0., 0.); //value and std error
        if(!segmentKins[i].empty())
        {
            pair<double,double> sum_kin_energy(0, 0); //value and std error
            sum_kin_energy = std::accumulate(segmentKins[i].cbegin(), segmentKins[i].cend(), sum_kin_energy, 
            [](const pair<double, double>& sum, const double& val)->pair<double,double>
              {
                  return make_pair(sum.first + val, sum.second + val*val);
              }
            );
    
            //const size_t nKinEnergies = segmentKins[i].size();
            const size_t nKinEnergies = dof[i];
            sum_kin_energy.first /= nKinEnergies;
            sum_kin_energy.second/= nKinEnergies;
            sum_kin_energy.second = sqrt(sum_kin_energy.second - sum_kin_energy.first*sum_kin_energy.first); //std dev
    
            temperature[i].first = kKinTemp * sum_kin_energy.first;
            temperature[i].second = sum_kin_energy.second * sqrt(kKinTemp / (nKinEnergies - 1)); //std error
        }
    }
}

/*
bool checkResName(t_atoms *at, const size_t index, const string resN)
{
	t_atom atom = at->atom[index];
	string resName  = string(*at->resinfo[atom.resind].name);	
	return resName.find(resN) == 0;
}
*/

bool checkAtomName(t_atoms *at, const size_t index, const string aName)
{
    string atomName = string(*at->atomname[index]);
    atomName.erase(std::remove(atomName.begin(), atomName.end(), ' '), atomName.end());
    return atomName == aName;
}

/*! \brief
 * The main function for the analysis template.
 */
int main(int argc, char *argv[])
{
	for(int i = 0; i < argc; ++i)
	{
		commandLineString+= argv[i];
		commandLineString+= " ";
	}

    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<temperatureAnalysisTool>(argc, argv);
}
