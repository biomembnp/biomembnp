This folder contains the comparison between atomistic and coarse-grained distributions of bonds, angles, and dihedrals.
CG0 and CG1 refer respectively to the first model and the second (refined) model.
