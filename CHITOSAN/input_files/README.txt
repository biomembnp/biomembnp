This folder contains some examples of input files:
- some itp files for short (decamers) and long (80-mers) CG chitosan chains
- a .gro file of a chitosan decamers

The polyply folder contains the input .ff files to use for generating topologies and starting coordinated with the Polyply software (Grünewald, Fabian, et al. "Polyply; a python suite for facilitating simulations of macromolecules and nanomaterials." Nature communications 13.1 (2022): 68.)
