;##################################################################
;# MARTINI topology of the 4 charged calcein dye
;#
;# Please read and cite: 
;# A Martini coarse-grained model of the calcein fluorescent dye
;# Salassi S., Simonelli F., Bartocci A., Rossi G.
;# J. Phys. D: Appl. Phys, 2018
;# https://doi.org/10.1088/1361-6463/aad4b8
;#
;# Last edit: 08/28/2017 by Salassi S.
;# Version: 1.5 
;##################################################################

; For minimization add 'define = -DFLEXIBLE' to the .mdp to use stiff bonds 

[moleculetype]
; molname   nrexcl
  CAL4         1

[ atoms ]
; id  type   resnr  residu  atom   cgnr charge
   1   Qa      1     CAL4    C1      1    -1
   2   Qa      1     CAL4    C5      2    -1
   3   Na      1     CAL4    N9      3     0
   4   Nda     1     CAL4    C4      4     0
   5   Nda     1     CAL4    O17     5     0
   6   SC4     1     CAL4    C26     6     0
   7   Nda     1     CAL4    C20     7     0
   8   SC4     1     CAL4    C25     8     0
   9   Na      1     CAL4    O36     9     0
  10   SC5     1     CAL4    R1     10     0
  11   SC5     1     CAL4    R2     11     0
  12   SC5     1     CAL4    R3     12     0
  13   Na      1     CAL4    N39    13     0 
  14   Qa      1     CAL4    C41    14    -1
  15   Qa      1     CAL4    C45    15    -1

[ bonds ]
; i  j  funct   length  force.c.
  1  3    1      0.30    12000
  2  3    1      0.30    12000
  3  4    1      0.36     8000
  6  9    1      0.305   10000
  7  13   1      0.36     8000 
  8  9    1      0.305   10000
  13 14   1      0.30    12000
  13 15   1      0.30    12000

#ifndef FLEXIBLE
[ constraints ]
; i  j  funct   length
  4  5    1      0.374
  4  6    1      0.267
  5  6    1      0.281
  5  7    1      0.374
  5  8    1      0.281
  7  8    1      0.266
  9  10   1      0.305
  9  12   1      0.236
  10 11   1      0.27
  10 12   1      0.27
  11 12   1      0.27
#else  ;for minimization
  4  5    1      0.374  30000
  4  6    1      0.267  30000
  5  6    1      0.281  30000
  5  7    1      0.374  30000
  5  8    1      0.281  30000
  7  8    1      0.266  30000
  9  10   1      0.305  30000
  9  12   1      0.236  30000
  10 11   1      0.27   30000
  10 12   1      0.27   30000
  11 12   1      0.27   30000
#endif

[ angles ]
; i  j  k   funct   angle  force.c.
  1  3  2     2      88      40
  1  3  4     2      82      45
  2  3  4     2      90      45
  3  4  5     2     128     300
  3  4  6     2      82     300
  4  6  9     2     180     200
  5  7  13    2     130     300
  5  9  10    2      55     300
  7  8  9     2     180     200
  7  13 14    2      82      45
  7  13 15    2      90      50
  8  7  13    2      82     300
  14 13 15    2      88      40

[ dihedrals ]
; i  j  k  l    funct   angle    forces.c.
  4  6  5  8      2     180.0      200
  5  6  8  9      2     180.0       80
  7  8  5  6      2     180.0      200
  9  12 10 11     2     180.0       80

[ exclusions ]
4 1 2 7 8 9
5 9 10
6 3 10
7 6 9 14 15
8 10 13
9 3 13 
