#!/usr/bin/env python
from __future__ import print_function
import numpy as np
from  math import *
import argparse
import sys
from random import randint
import subprocess


#FUNCTIONS

def parseITP(itpFile):
    lines = open(itpFile).readlines();
    itp = {}
    appender = ""
    for i in range(0, len(lines)):
        strippedLine = lines[i].strip()
        if not strippedLine or strippedLine.startswith('#') or strippedLine.startswith(';') or strippedLine.startswith('\n'):
            continue

        if '[' in lines[i] and ']' in lines[i]:
            lines[i]=lines[i].replace(" ", "")
            appender = lines[i][1:-2]
            itp[appender] = []
        else:
            if appender == "moleculetype":
                itp[appender] = lines[i]
            else:
                itp[appender].append(lines[i].split())
    return itp         

def xyz2rtp(xyz):
    r = sqrt( xyz[0]**2 + xyz[1]**2 + xyz[2]**2 )
    t = acos( xyz[2] / r ) 
    p = abs(atan( xyz[1] / xyz[0] ))

    if xyz[0] > 0 and xyz[1] < 0 and xyz[2] > 0:
        p = 2*np.pi - p
    elif xyz[0] < 0 and xyz[1] < 0 and xyz[2] > 0:
        p =   np.pi + p
    elif xyz[0] < 0 and xyz[1] > 0 and xyz[2] > 0:
        p =   np.pi - p
    elif xyz[0] > 0 and xyz[1] < 0 and xyz[2] < 0:
        p = 2*np.pi - p
    elif xyz[0] < 0 and xyz[1] < 0 and xyz[2] < 0:
        p =   np.pi + p
    elif xyz[0] < 0 and xyz[1] > 0 and xyz[2] < 0:
        p =   np.pi - p

    return [r,t,p]

def rtp2xyz(rtp):
    x = rtp[0] * sin(rtp[1]) * cos(rtp[2])
    y = rtp[0] * sin(rtp[1]) * sin(rtp[2])
    z = rtp[0] * cos(rtp[1])

    return [x,y,z]

def printForceConstat(list):
    val = []
    for x in list:
        try:
            val.append(float(x))
        except ValueError:
            return "  ".join(list)

    return "".join(["% 9.3f "%x for x in val])

def parseGroFile(filename):
    file = open(filename, 'r').readlines()
    comment = file.pop(0) # skip comment line
    file.pop(0) # skip number of atoms
    box = [float (x) for x in file.pop(-1).strip().split()] # skip box line

    groFile = []
    for line in file:
        groFile.append([int(line[:5]), line[5:10].strip(), line[10:15].strip(), int(line[15:20].strip())] + [float(i) for i in line[20:].split()][:3])

    return comment,groFile,box

def parseXYZFile(filename):
    auNPCluster = open(filename, 'r')
    auNPCluster.readline()
    auNPCluster.readline() # remove comment line in the xyz file
    atoms=[] # we want in gro format!
    i=1
    for line in auNPCluster:
        temp = line.split()
        atoms.append([1, "NP", temp[0], i] + [float(x)/10. for x in temp[1:4]])
        i+=1
    auNPCluster.close()

    return atoms

#MAIN SCRIPT

parser = argparse.ArgumentParser()
parser.add_argument("-f", required=True, type=str, dest="aunp", 
    help="Input .xyz or .gro of AuS NP.")
parser.add_argument("-p1", required=True, type=str, dest="aunptop", 
    help="AuS topology file [.itp]")
parser.add_argument("-p2", required=True, type=str, nargs="+", dest="ligandtop", 
    help="Ligand topology files, space separeted [.itp]")
parser.add_argument("-box", type=float, nargs="+", dest="box", 
    help="Box dimensions of the output configuration (x y z) [nm]; if not specified they will be guessed")
parser.add_argument("-o",  default="out.gro", type=str, dest="outputFile", 
    help="Output .gro file.")
parser.add_argument("-op", default="out.itp", type=str, dest="outputItp", 
    help="Output .itp file.")
parser.add_argument("-cg", action='store_true', 
    help="If it is a coarse-grained topology.")
parser.add_argument("-n", default=[100], nargs="+", type=float, dest="conc",
    help="Ligands concentration, space separeted [%%]")
parser.add_argument("--random", action='store_true', 
	help="Put ligands in a random arrangements")

parser.add_argument("--gmx", default="none", type=str, 
	help="gmx program to use for minimizations, if 'none' just attach ligands. WARNING! Curently working only for the OPLS forcefield.")
parser.add_argument("--solvent", default="none", type=str,
	help="Name of the solvent model, 'none' means no water")

#Advanced options
parser.add_argument("--atom-distance", dest="atomdistance", default=0., type=float,
    help="Advanced! Distance between atoms in the output configuration [nm].")
parser.add_argument("--nrexcl", default=0, type=int,
    help="Advanced! 'nrexcl' argument in the output topology.")

args = parser.parse_args()

if args.box is not None and len(args.box) < 3:
    print("Error! '-box' option need 3 argument!")
    exit(1)

ligandsType = 1
if args.conc is not None:
    if len(args.ligandtop) == 1:
        print("Warning! Only one type of ligand: the option '-n' will be ignored!")
    elif len(args.conc) != len(args.ligandtop):
        print("Error! Expecting '" + str(len(args.ligandtop)) + "' ligand concentration values!")
        exit(1)
    else:
        ligandsType = len(args.ligandtop)

for top in args.ligandtop:
    if ".itp" not in top:
        print("Error! Topology file '" + top + "' not supported!")
        exit(1)

#Read input core file
atoms = [] # in gro format!
groBox = []
if ".xyz" in args.aunp:
    atoms = parseXYZFile(args.aunp)
elif ".gro" in args.aunp:
    comment,atoms,groBox = parseGroFile(args.aunp)
else:
    print("Error! Input file '", args.aunp, "' not supported!")
    exit(1)

clusterSize = len(atoms)

#Parse ligand and core topology (gromacs format)
ligandTop = [parseITP(x) for x in args.ligandtop]   #parseITP(args.ligadtop[0])
npTop = parseITP(args.aunptop)

for i in range(0, len(ligandTop)):
    if "moleculetype" not in ligandTop[i]:
        print("Error! No molecule found in '", args.ligandtop[i], "'!")
        exit(1)

if "moleculetype" not in npTop:
    print("Error! No molecule found in '", args.aunptop)
    exit(1)

ligandResName = [x["moleculetype"].split()[0] for x in ligandTop] # ligandTop["moleculetype"].split()[0]
atomPerLigand = [len(x["atoms"]) for x in ligandTop]  # len(ligandTop["atoms"])
ligandName= [[x[4] for x in y["atoms"] ] for y in ligandTop ]  #  [x[4] for x in ligandTop["atoms"] ]

print("Found the following ligands: ")
print("    "+", ".join(ligandResName))

com = [reduce((lambda x, y: x+y), x) for x in zip(*atoms)[4:7]]
com = [x/clusterSize for x in com]

# Remove COM
for i in range(0, clusterSize):
    atoms[i][4] -= com[0] 
    atoms[i][5] -= com[1]
    atoms[i][6] -= com[2]

sAtoms = len(filter(lambda x: x[4] == "S", npTop["atoms"]))
auAtoms = len(filter(lambda x: x[4] == "Au", npTop["atoms"]))

verifyS = len(filter(lambda x: x[2] == "S", atoms))
verifyAu = len(filter(lambda x: x[2] == "Au", atoms))

#Verify S and Au atoms
if sAtoms != verifyS:
    print("Sulfur atoms mismatch between '"+args.aunp+"' and NP topology!")
    exit(1)
print("Found "+str(sAtoms) +" sulfur atoms in molecule type "+npTop["moleculetype"].split()[0])
if auAtoms != verifyAu:
    print("Gold atoms mismatch between '"+args.aunp+"' and NP topology!")
    exit(1)
print("Found "+str(auAtoms)+" gold atoms in molecule type "+npTop["moleculetype"].split()[0])

#Number of ligands
ligandsNumber = [int(round(x*sAtoms/100, 0)) for x in args.conc]

atomLigandDistance = 0.15 # CT-CT bonds length (from OPLS GROMACS file)
if args.cg:
    atomLigandDistance = 0.47 # CG Martini sigma
if args.atomdistance != 0:
    atomLigandDistance = args.atomdistance

###########################
#Add ligands in S position#
###########################
sRange = range(auAtoms, clusterSize) 
# should be +1 but sRange[x] is the position to get coordinate from atoms array so it need to be -1
if args.random:
    #np.random.seed(1) # for test
    sRange = list(np.random.choice(sRange, size=sAtoms, replace=False))

sAtoms2Ligands = [None]*sAtoms

# cycle over type of ligands
for i in range(0, len(ligandTop)):
    # cycle over number of ligands per type
    for j in range(0, ligandsNumber[i]): 
        pos = j+sum(ligandsNumber[:i]) #index of sulfur position
        s = sRange[pos]
        sAtoms2Ligands[pos] = s+1
        sulpurRtp = xyz2rtp(atoms[s][4:7])
        # cycle over atoms per ligand
        for k in range(1, atomPerLigand[i]): #starting from 1 to exclude S in ligand .ipt
            xyz = rtp2xyz( [ sulpurRtp[0]+k*atomLigandDistance, sulpurRtp[1], sulpurRtp[2] ] )
            atoms.append( [ pos+2, ligandResName[i], ligandName[i][k], len(atoms)+1 ] + [float(x) for x in xyz] )
                             # +1 for NP resnumber, +1 to get real resnumber from array position

nAtoms = len(atoms)
print("Attached: ")
for i in zip(ligandsNumber, ligandResName):
    print("   "+str(i[0])+" ligands of type "+i[1])

print("Final number of particles: "+str(nAtoms))

#If boxes are not specified, guess
box = []
if args.box is not None:
    box = args.box
elif groBox:
    box = groBox
else:
    r = [np.array(np.array(atoms)[:, x], dtype=float) for x in [4,5,6]]
    maxB=np.array([np.amax(x) for x in r])
    minB=np.array([np.amin(x) for x in r])
    box = maxB + np.abs(minB) + 2
    print("Box dimensions not specified, guessed as: " + "(%6.4f, %6.4f, %6.4f)"%(tuple(box)))

#Center respect to center of box
for i in atoms:
    i[4] += box[0]/2.
    i[5] += box[1]/2.
    i[6] += box[2]/2.

outGro=args.outputFile
if ".gro" not in outGro:
    print("WARNING! The output configuration file should be a .gro file!")
    outGro += ".gro"
    print("Setting the output configuration file as "+outGro)

###################
####Writing GRO####
###################
out = open(outGro, 'w')
out.write("AU"+str(auAtoms)+"S"+str(sAtoms)+"_"+"_".join(ligandResName)+"\n")
out.write(str(nAtoms)+"\n")
#Coordinates
for i in range(0, nAtoms):
    out.write("%5d%-5s%5s%5d%8.3f%8.3f%8.3f\n"%(tuple(atoms[i])))
#Box dimensions
out.write("%8.4f %8.4f %8.4f\n"%(tuple(box)))
out.close()

########################
####Writing TOPOLOGY####
########################
outItp=args.outputItp
if ".itp" not in outItp:
    print("WARNING! The output topology file should be a .itp file!")
    outItp += ".itp"
    print("Setting the output topology file as "+outItp)
out = open(outItp, 'w')

#####################
#Write Molecule type#
#####################
out.write("\n; Topology generated by the following command:\n;")
out.write(" ".join(sys.argv))
out.write("\n\n[moleculetype]\n")
out.write(";  molname      nrexcl\n")

totalNPCharge = 0;
#Exclusion (depends on ff)
nrexcl = 3
if args.cg:
    nrexcl = 1
if args.nrexcl != 0:
    nrexcl = args.nrexcl
molName = "AU_"+"_".join(ligandResName)
out.write("%10s%10i\n\n"%(molName, nrexcl))
out.write("[atoms]\n")
out.write(";  id     type  resnr  resname   atom  cgnr  charge\n")
out.write("; NP core\n")
for i in npTop["atoms"]:
    out.write(" %4s%9s%7s%7s%7s%6s%12s\n"%(tuple(i)))
    totalNPCharge+= float(i[6])
index = clusterSize
for i in range(0, len(ligandTop)):
    for j in range(0, ligandsNumber[i]):
        resnr = j+sum(ligandsNumber[:i])
        out.write("; ligand %3i %s\n"%(resnr+1, ligandTop[i]["atoms"][0][3]))
        for k in ligandTop[i]["atoms"]:
            if k[4] == "S": continue
            index+= 1
            out.write(" %4i%9s%7i%7s%7s%6i%12s\n"%(index, k[1], resnr+2, k[3], k[4], index, k[6]))
            totalNPCharge+= float(k[6])

###############
#####BONDS#####
###############
out.write("\n")
out.write("[bonds]\n")
out.write(";   i    j    f     b0(nm)  k(kJ/(mol.nm^2))\n; Au-Au, S-S and Au-S elastic network\n")
#NP elastic network
for i in npTop["bonds"]:
    out.write("%5s%5s    %s\n"%(i[0], i[1], "  ".join(i[2:])))

out.write("\n; Ligand bonds\n")
index = clusterSize + 1
for i in range(0, len(ligandTop)):
    for k in range(0, ligandsNumber[i]):
        resnr = k+sum(ligandsNumber[:i])
        out.write("; ligand %3i, %s\n"%(resnr+2, ligandResName[i]))
        for j in ligandTop[i]["bonds"]:
            if ligandTop[i]["atoms"][int(j[0])-1][4] == "S":
                # S - R bonds
                out.write("%5i%5i%5i %s\n"%(int(j[0])-1+sAtoms2Ligands[resnr], #-1 for S atoms in ligand topology
                                            index,
                                            int(j[2]),
                                            printForceConstat(j[3:])))
            else: #Other ligand bonds    
                out.write("%5i%5i%5i %s\n"%(int(j[0])-2+index, 
                                            int(j[1])-2+index, 
                                            int(j[2]),
                                            printForceConstat(j[3:])))
        index+= atomPerLigand[i]-1


################
#####ANGLES#####
################
out.write("\n")
out.write("[angles]\n")
out.write(";   i    j    k    f\n")
index = clusterSize + 1
for i in range(0, len(ligandTop)):
    for k in range(0, ligandsNumber[i]):
        resnr = k+sum(ligandsNumber[:i])
        out.write("; ligand %3i, %s\n"%(resnr+2, ligandResName[i]))
        for j in ligandTop[i]["angles"]:
            if ligandTop[i]["atoms"][int(j[0])-1][4] == "S":
                # S - R
                out.write("%5i%5i%5i%5i %s\n"%(int(j[0])-1+sAtoms2Ligands[resnr],
                                        index,
                                        int(j[2])-2+index,
                                        int(j[3]),
                                        printForceConstat(j[4:])))
            else: #Other ligand bonds
                out.write("%5i%5i%5i%5i %s\n"%(int(j[0])-2+index, 
                                        int(j[1])-2+index,
                                        int(j[2])-2+index,
                                        int(j[3]),
                                        printForceConstat(j[4:])))
        index+= atomPerLigand[i]-1

###################
#####DIHEDRALS#####
###################
if "dihedrals" in [y for x in ligandTop for y in x.keys()]:
    out.write("\n")
    out.write("[dihedrals]\n")
    out.write(";   i    j    k    l    f\n")
    index = clusterSize + 1
    for i in range(0, len(ligandTop)):
        if "dihedrals" not in ligandTop[i]:
            continue

        for k in range(0, ligandsNumber[i]):
            resnr = k+sum(ligandsNumber[:i])
            out.write("; ligand %3i, %s\n"%(resnr+2, ligandResName[i]))
            for j in ligandTop[i]["dihedrals"]:
                if ligandTop[i]["atoms"][int(j[0])-1][4] == "S":
                    #S - R
                    out.write("%5i%5i%5i%5i%5i %s\n"%(int(j[0])-1+sAtoms2Ligands[resnr],
                                                index,
                                                int(j[2])-2+index,
                                                int(j[3])-2+index,
                                                int(j[4]),
                                                printForceConstat(j[5:])))
                else: #Other ligand bonds
                    out.write("%5i%5i%5i%5i%5i %s\n"%(int(j[0])-2+index, 
                                                int(j[1])-2+index,
                                                int(j[2])-2+index,
                                                int(j[3])-2+index,
                                                int(j[4]),
                                                printForceConstat(j[5:])))
            index+= atomPerLigand[i]-1

###############
#####PAIRS#####
###############
if "pairs" in [y for x in ligandTop for y in x.keys()]:
    out.write("\n")
    out.write("[pairs]\n")
    out.write(";   i    j    f\n")
    index = clusterSize + 1
    for i in range(0, len(ligandTop)):
        if "pairs" not in ligandTop[i]:
            continue

        for k in range(0, ligandsNumber[i]):
            resnr = k+sum(ligandsNumber[:i])
            out.write("; ligand %3i, %s\n"%(resnr+2, ligandResName[i]))
            for j in ligandTop[i]["pairs"]:
                if ligandTop[i]["atoms"][int(j[0])-1][4] == "S":
                    #S - R
                    out.write("%5i%5i%5i %s\n"%(int(j[0])-1+sAtoms2Ligands[resnr],
                                            int(j[1])-2+index,
                                            int(j[2]),
                                            printForceConstat(j[3:])))
                else: #Other ligand bonds
                    out.write("%5i%5i%5i %s\n"%(int(j[0])-2+index, 
                                            int(j[1])-2+index,
                                            int(j[2]),
                                            printForceConstat(j[3:])))
            index+= atomPerLigand[i]-1

out.close()

print("NP succesfully created!")
print("")

if args.gmx is not "none":

	minMdp='\
integrator               = steep\n\
emtol                    = 10\n\
emstep                   = 0.01\n\
nsteps                   = 1000000\n\
cutoff-scheme            = Verlet\n\
vdwtype                  = Cut-off\n'

	if args.cg:
		minMdp+='\
coulombtype              = cut-off\n\
rcoulomb                 = 1.1\n\
epsilon_r                = 15\n\
rvdw                     = 1.1\n'
	else:
		minMdp+='\
rvdw                     = 1.0\n\
coulombtype              = PME\n\
rcoulomb                 = 1.0\n'


	with open("min.mdp", 'w') as min:
		min.write(minMdp)
 
	topolTop = '#include '
	if args.cg:
		topolTop+= '"martini_v2.2.itp"\n'
	else:
		topolTop+='"forcefield.itp\n'
	topolTop+='\
#include "'+args.outputItp+'"\n\n\
[ system ]\n'+molName+'\n\n\
[ molecules ]\n'+molName+' 1\n'

	with open("topol_vacum.top", 'w') as topol:
		topol.write(topolTop)

	print("[1] Minimizing in vacum...")
	# gmx grompp
	cmd=args.gmx + " grompp -f min.mdp -c " + args.outputFile + " -p topol_vacum.top -o min_vacum.tpr -maxwarn 10"
	subprocess.call(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	#gmx mdrun
	cmd=args.gmx + " mdrun -nt 4 -pin on -deffnm min_vacum"
	subprocess.call(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if args.solvent is not "none":
		print("[2] Solvate with water")
		solventTable = {"spc": "spc216", "spce": "spc216", "tip3p": "spc216", "tip4p": "tip4p", "tip5p": "tip5p"}
		#gmx solvate
		cmd=args.gmx + " solvate -cp min_vacum.gro -cs " + solventTable[args.solvent] + " -o NPWater.gro"
		pipes = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		std_out, std_err = pipes.communicate()
		#print(std_err)
		#print(std_err.rpartition("Number of solvent molecules"))
		solMolecules = filter(str.isdigit, std_err.partition("Number of solvent molecules")[2])
		print("    Solvated with ", solMolecules, " water molecules.")

		#print("Total NP charge ", totalNPCharge)
		#ions = [abs(int(totalNPCharge)), "NA", True] # True = positive, False = negative
		#if int(totalNPCharge) > 0:
		#	ions[1] = "CL"
		#	ions[2] = False
		
		topolTop = '\
#include "forcefield.itp\n\
#include "oplsaa.ff/'+args.solvent+'.itp"\n\
#include "'+args.outputItp+'"\n\n\
[ system ]\n'+molName+'\n\n\
[ molecules ]\n'+molName+' 1\n\
SOL '+solMolecules+'\n'
		
		with open("topol.top", 'w') as topol:
			topol.write(topolTop)
		
		print("[3] Minimizing in water...")
		# gmx grompp
		cmd=args.gmx + " grompp -f min.mdp -c NPWater.gro -p topol.top -o min.tpr -maxwarn 10"
		subprocess.call(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		
		#gmx mdrun
		cmd=args.gmx + " mdrun -nt 4 -pin on -deffnm min"
		subprocess.call(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print("")
print("Done")
print("")

