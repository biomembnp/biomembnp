#!/bin/bash

rm pull_tpr.dat
rm pull_x.dat

for i in $(cat listw)
do
	ls $i/us/us.tpr >> pull_tpr.dat
	ls $i/us/us_pullx.xvg >> pull_x.dat
done
