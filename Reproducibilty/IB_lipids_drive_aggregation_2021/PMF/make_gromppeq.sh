#!/bin/bash

for i in $(cat listw)
do
	(
		cd $i/eq
       		gmx_cuda grompp -f eq.mdp -c eq2.gro -p topol.top -n index.ndx -o eq.tpr &> grompp_eqlog
		if [[ -e eq.tpr ]]; then
		    echo "$i successful."
		else
		    echo "$i failed."
		fi

	)
done
