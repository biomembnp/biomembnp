#!/bin/bash

for i in $(cat listw)
do
	sed "s/WIN/$i/" < us_template.mdp > $i/us/us.mdp
	cp init/*.itp $i/us/
	cp init/*.top $i/us/
	cp init/*.ndx $i/us/
	cp $i/eq/eq2.gro $i/us/
done
