#!/bin/bash

for i in $(cat listw)
do
	(
		cd $i/us
       		gmx_cuda grompp -f us.mdp -c eq2.gro -p topol.top -n index.ndx -o us.tpr &> grompp_uslog
		if [[ $? -eq 0 ]]; then
			echo "$i OK"
		else
			echo "$i ERROR"
		fi
	)
done
