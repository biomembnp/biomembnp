#!/bin/bash

for i in $(cat listw)
do
	sed "s/WIN/$i/" < eq_template.mdp > $i/eq/eq.mdp
	cp init/*.itp $i/eq/
	cp init/*.top $i/eq/
	cp init/*.ndx $i/eq/
	cp eq1/eq2.gro $i/eq/
done
